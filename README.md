
# Seamonjs SDK

New module friendly JS implementation of the Seamonjs SDK.
To run the development test, execute the following,

```
$ npm install
$ npm run start
```

The SRC development is developed in ES6 modules under `./esm` directory.
I.e. only edit the esm files.
To deploy commonjs files for module import in other JS application do the following
before committing to repository,

```
$ npm run buildcjs
```

Then commit any changes to the repository,

```
$ git add ./esm
$ git add ./cjs
$ git commit
```

## How to use

Add the seamonjs to your dev project,

```
$ npm i "git+https://gitlab.com/bitvinci/seamonjs.git" -S
```

In your project import the SeaMon api methods,

```javascript
import SeaMon from 'seamonjs';
```

**TODO**: Add link to new docs
