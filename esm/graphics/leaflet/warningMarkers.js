import L from 'leaflet';
import { generateCritical, stylesState } from '../../styles/styles';

// generate warning markers
function warningMarkers(data, model, map, opts={}) {

    let { critical, encs } = generateCritical(data);

    // pick out data
    let msgs = encs;
    let lats = data.forecasts[model].lats;
    let lons = data.forecasts[model].lons;
    let style = stylesState.criticalStyle;

    // generate markers into feature group
    let fg = L.featureGroup();
    let prev_i = 0;
    let prev_msg = null;
  
    // collect some display settings...
    let eOpts = {}
    if('pane' in opts) eOpts.pane = opts.pane;

    // NOTE: Assume msgs come sorted ...
    msgs.forEach(x => {
      let i = x.index;
  
      // construct warnig msg -- from array
      let msg = "";
      x.msgs.forEach(m => {
        m = m.split(',').join('<br/>');
        msg += m + '<br/>';
      });
  
      // if duplicate of previous one within 10 steps abort
      if( i-prev_i < 10 ) {
        if(msg === prev_msg) return;
      }
  
      prev_i = i;
      prev_msg = msg;
  
      // create marker
      // set svg icon
      let icon = `<svg style="fill:${style[x.critical].color};
           filter: drop-shadow(0px 0px 2px black);"
           xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
        <path d="M0 0h24v24H0z" fill="none"/>
        <path d="M 12,2 L 23,21 L1,21" fill="black"/>
        <path d="M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z"/>
      </svg>`;
  
      //let icon = `<svg style="fill:${style[x.critical].color}" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="white"/><path d="M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z"/></svg>`;
      let PosIcon = L.icon({
          iconUrl: 'data:image/svg+xml;base64,' + btoa(icon),
          iconSize: [30, 30]
      });
  
      // marker
      let mark = L.marker(new L.LatLng(lats[i], lons[i]), {icon:PosIcon,
                                                           /*zIndexOffset:-10,*/
                                                           ...eOpts});
      let pop = L.popup({closeButton: false,
                         className: 'warnlabel'}).setContent(`${msg}`);
      mark.bindPopup(pop, {autoClose: false,
                           closeOnClick: false})
          .openPopup().addTo(fg);
    });
  
    if(map) fg.addTo(map);
    return fg;
  }
  
  export default warningMarkers;