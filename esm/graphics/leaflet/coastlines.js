// quick and dirty coaslines plotting
// for leaflet based on geojson blob
// TODO: implement a more flexible coastlines service within seamon.
import L from 'leaflet';

function coastlines(map, bbox, opts) {

    // get the coaslines geojson
    let url = 'https://api.seamon.io/mapdata/ne_10m_coastline.geojson';

    return fetch(url)
    .catch(e => {
        console.error(`fetch had error in ${fcmethod}`, e);
        return Promise.reject(new Error("failed to reach server - check internet connection"));
    })
    .then(resp => {
        // check here for unexpected HTTP errors . e.g. 502
        if ([502, 504].includes(resp.status)) {
        return Promise.reject(new Error(`${resp.status} gateway error - service may be down - admin will be notified`));
        }
        // all ok so pass on...
        return resp;
    })
    .then(resp => resp.json()) // converts to json
    .then(json => {
        console.log("got geojson data", json);
        // clip the coastline data by bbox
        let crop = cropCoastlines(json, bbox);
        // add to map
        L.geoJSON(crop, opts).addTo(map);    
    });
}

function cropCoastlines(d, bbox) {
    let minLon = bbox[0];
    let minLat = bbox[1];
    let maxLon = bbox[2];
    let maxLat = bbox[3];
    let c = {};
    c.bbox = bbox;
    c.type = d.type;
    c.features = [];

    for(let i=0; i<d.features.length; i++){
        let x = d.features[i];
        let coords = [[]]; // array of coord arrays
        let iSeg=0;
        // loop through coords checking if in bbox
        // and break up features that are split by cropping
        x.geometry.coordinates.forEach(ll => {
            let lon = ll[0];
            let lat = ll[1];
            if( (minLon<lon)&&(lon<maxLon)&&(minLat<lat)&&(lat<maxLat) ) {
                coords[iSeg].push(ll);
            } else {
                if(coords[iSeg].length > 0) 
                {
                    iSeg += 1;
                    coords[iSeg] = [];
                }
            }
        });
        coords.forEach( co => {
            if( co.length > 1 ) {
                let f = {type: x.type, properties: x.properties};
                f.bbox = x.bbox;
                f.geometry = {type: x.geometry.type, coordinates: []};
                f.geometry.coordinates = co;
                c.features.push(f);
            }
        })
    }
    return c;
}

export default coastlines;