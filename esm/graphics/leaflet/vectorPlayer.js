import L from 'leaflet';

class VectorPlayer {

    constructor(data, model, ampPar, dirPar, map=null, opts={}) {
        this._class_name = "VectorPlayer";

        // opts defaults
        if(!opts.weight) opts.weight = 2.0;
        if(!opts.color) opts.color = "black";
        if(!opts.size) opts.size = 1.0;
        if(!opts.length) opts.length = 1.0;
        if(!opts.minAmp) opts.minAmp = null;
        if(!opts.maxAmp) opts.maxAmp = null;
        if(!opts.featherOpacity) opts.featherOpacity = null;
        if(!opts.opacity) opts.opacity = 1.0;
        if(!opts.pane) opts.pane = null;

        // TODO: add forecast response type check
        // because this method only applies to map response data

        // create vector features...
        this.vectors = createVectors(data, model, ampPar, dirPar, map, opts)

        // collect time stamps
        let times = data.forecasts[model].times;
        let timesJS = [];
        times.forEach(t => {
            timesJS.push(new Date(t*1000.0));
        });

        // store JS time stamps
        this.times = times;
        this.timesJS = timesJS;
        this.idx = 0; // first vector image displayed

        // if map provided add ol to map
        this.maps = [];
        if(map) {
            this.vectors[this.idx].addTo(map);
            // keep track of attached maps...
            this.maps.push(map);
        }

        // create time step lookup,
        let timeLookup = SeaMon.lookup.timeStepLookup(times);
        this._timeLookup = timeLookup;
    }
    length() {
        return this.times.length;
    }
    getBounds() {
        return this.vectors[0].getBounds();
    }
    setIndex(idx) {
        if(idx > this.length()) {
            console.error(this._class_name + `:cant show, index ${idx} exceeds length of forecast`);
            return;
        }
        let prevIdx = this.idx;
        this.maps.forEach((m)=>{
            // flip vectors...
            console.log("TODO, flip vectors");
            m.removeLayer(this.vectors[prevIdx]);
            this.vectors[idx].addTo(m);
        });
        this.idx = idx;
    }
    lookup(t){
        return this._timeLookup.nearest({x:t}, 1)[0][0].idx;
    }
    setTime(t) {
        let idx = this.lookup(t);
        this.setIndex(idx);
    }
    time() { return this.times[this.idx]; }
    timeJS() { return this.timesJS[this.idx]; }
    next() {
        this.idx = (this.idx + 1)%this.length();
        this.setIndex(this.idx);
    }
    addTo(map) {
        this.vectors[this.idx].addTo(map);
        // keep track of attached maps...
        this.maps.push(map);
    }
    remove(map=null) {
        if(map) {
            map.removeLayer(this.vectors[this.idx]);
            this.maps = this.maps.filter(item => item !== map);
            return;
        }
        // else remove from all maps in the maps array,
        this.maps.forEach(x => {
            x.removeLayer(this.vectors[this.idx]);
        });
        this.maps = [];
    }
}

function vectorPlayer(data, model, ampPar, dirPar, map, opts={}) {
    let player = new VectorPlayer(data, model, ampPar, dirPar, map, opts);
    return player;
}

function createVectors(data, model, ampPar, dirPar, map, opts={}) {
    let funcName = "createVectors:";

    // opts defaults
    if(!opts.weight) opts.weight = 2.0;
    if(!opts.color) opts.color = "black";
    if(!opts.opacity) opts.opacity = 1.0;
    if(!opts.size) opts.size = 1.0;
    if(!opts.length) opts.length = 1.0;
    if(!opts.minAmp) opts.minAmp = null;
    if(!opts.maxAmp) opts.maxAmp = null;
    if(!opts.featherOpacity) opts.featherOpacity = null;

    // model data
    let fcDat = data.forecasts[model];

    // check if data available
    if(!('lats' in fcDat)) { 
        console.error(funcName+"no lat grid available in data");
        return null;
    }
    if(!('lons' in fcDat)){
        console.error(funcName+"no lon grid available in data");
        return null;
    }
    if((ampPar) && !(ampPar in fcDat.members[model])){
        console.error(funcName+"amplitude paremeter not in data");
        return null;
    }
    if(!(dirPar in fcDat.members[model])){
        console.error(funcName+"direction paremeter not in data");
        return null;
    }

    // basic vector shape features...
    // TODO: check geographic meaning of sizes in leaflet...
    let baseSize = 200.0;
    let size = opts.size*baseSize;
    let length = opts.length; // length factor for vectors

    // using vector amplitudes?
    let usingAmps = false;
    let minAmp = opts.minAmp; // skip drawing if < this amplitude
    let maxAmp = opts.maxAmp; // skip drawing if > this amplitude
    let featherOpacity = opts.featherOpacity;
    let opacity = opts.opacity;
    let ampMax = 0.0;
    if(ampPar) {
        usingAmps = true;
        // get the max from first time step...
        // helps normalize the vectors w.r.t. size option.
        // TODO: this could be improved...
        ampMax = Math.max(...fcDat.members[model][ampPar][0]);
        console.log("max amp is", ampMax);
    }

    let lats = data.forecasts[model].lats;
    let lons = data.forecasts[model].lons;
    let Nsteps = data.forecasts[model].times.length;

    // create arrow line features for leaflet
    let fGroups = [];
    for(let i=0; i<Nsteps; i++) {
        // always use direction param
        let dir = fcDat.members[model][dirPar][i];
        // use amplitude of vector if provided
        let amp = null;
        if(usingAmps) amp = fcDat.members[model][ampPar][i];
        
        let fg = L.featureGroup();

        // loop through wind data creating lines
        let arrowSize = size;
        let wingSize1 = size/8.0;
        let wingSize2 = size/3.0;

        for(let i=0; i<dir.length; i++) {
            // if null 'masked' map value,
            // then skip forward to next vector,
            if(dir[i] == null) continue;

            // start point in latlon
            let ll0 = {lat:lats[i], lng:lons[i]};
            // always use direction parameter
            let theta = dir[i]*Math.PI/180.0;
            // use amplitude if provided
            let A = null;
            let thisOpacity = opacity;
            if(amp) {
                A = amp[i]/ampMax; // normalize length of vector
                if(minAmp && amp[i]<minAmp) continue;
                if(maxAmp && amp[i]>maxAmp) continue;
                if(featherOpacity) {
                    if(amp[i]>featherOpacity) thisOpacity = opacity;
                    else {
                        thisOpacity = amp[i]/featherOpacity*opacity;
                    }
                }
            }

            //// Draw arrows in target projection
            //// Ensures they are sized by the map (not latlon)

            // project arrow origin point to map grid
            let p0 = map.project(ll0,10);
            
            // stem point of arrow,
            // affected by amplitude if provided
            let thisArrowLen = arrowSize*length;
            if(A) {
                thisArrowLen *= A;
            }

            let p1 = {x: p0.x, y: p0.y+thisArrowLen};
            // wing of arrow
            let w1 = {x: p0.x+wingSize1, y: p0.y+wingSize2};
            let w2 = {x: p0.x-wingSize1, y: p0.y+wingSize2};
            // rotate
            p1 = rotate(p0, p1, theta);
            w1 = rotate(p0, w1, theta);
            w2 = rotate(p0, w2, theta);
            
            // project back to second point in latlon
            let ll1 = map.unproject(p1,10);
            let wll1 = map.unproject(w1,10);
            let wll2 = map.unproject(w2,10);

            // create lines
            let stem = new L.Polyline([ll0, ll1],{
                color: opts.color,
                weight: opts.weight,
                opacity: thisOpacity,
            });
            let head = new L.Polyline([wll1, ll0, wll2],{
                color: opts.color,
                weight: opts.weight,
                opacity: thisOpacity,
            });
            if(opts.pane) {
                stem.options.pane = opts.pane;
                head.options.pane = opts.pane;
            }
            stem.addTo(fg);
            head.addTo(fg);
        }
        fGroups.push(fg);
    }

    return fGroups;
}

// roatate point p1 w.r.t. p0
function rotate(p0, p1, angle) {
    let S = Math.sin(angle);
    let C = Math.cos(angle);

    let dx = p1.x - p0.x;
    let dy = p1.y - p0.y;

    let dxr = dx*C + dy*S;
    let dyr = dx*S - dy*C;

    return {x:p0.x+dxr, y:p0.y+dyr};
}

export default vectorPlayer;