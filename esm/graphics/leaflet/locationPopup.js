import L from 'leaflet';
import dateFormat from 'dateformat';

/* displays FC data in a popup in leaflet
   should be used with location forecasts */
function locationPopup(data, map, step=0, time=null) {

    // check...
    if(!('forecasts' in data)) {
        console.error('locationPopup: no forecast data to show');
        return null;
    }

    // find first forecast with any data
    let firstFC = null;
    for(let fc in data.forecasts ) {
        if(data.forecasts[fc].times.length > 0) {
            firstFC = fc;
            break;
        }
    }
    if(firstFC == null) {
        console.error("locationPopup: no data found");
        return null;
    }

    // get location
    let lat = data.forecasts[firstFC].lat;
    let lon = data.forecasts[firstFC].lon;
    let l = {lat: lat, lng: lon};

    // get time,
    time = data.forecasts[firstFC].times[step];
    let jsTime = new Date(1000.0*time);

    // loop through forecast data,
    // creating html summary for popup
    let c = '';
    // add timestamp,
    c += '<span style="font-weight:bold;">'+dateFormat(jsTime, "dd mmm HH:MM")+"</span>";
    c += '<hr style="margin:0;padding:0;"/>';
    for(let fc in data.forecasts) {
        let fcData = data.forecasts[fc];
        let units = fcData.units;
        c += `<span style='font-weight:bold;'>${fc.toUpperCase()}</span><br/>`;
        for(let par in fcData.members[fc]) {
            let val = fcData.members[fc][par][step];
            let unit = "";
            if(par in units) unit = units[par];
            c += `- <span style='font-weight:bold;'>${par}:</span> ${val}${unit}<br/>`
        }
        /* check if warning in data */
        if(fcData.warnings.length)
            console.warn('locationPopup:', fc, fcData.warnings);
        /* if no data in this fc */
        if(fcData.times.length === 0) {
            c += "no data<br/>";
        }
    }

    var popup = L.popup()
    .setLatLng(l)
    .setContent(c)
    .openOn(map);

}

export default locationPopup;