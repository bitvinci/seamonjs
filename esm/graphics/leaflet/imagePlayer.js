import L from 'leaflet';
import timeStepLookup from '../../lookup/lookup';

class ImagePlayer {
    
    constructor(data, model, par, map=null, opts={}) {
        this._class_name = "ImagePlayer";

        // opts defaults
        if(!opts.opacity) opts.opacity = 1.0;
        if(!opts.pane) opts.pane = null;

        // TODO: add forecast response type check
        // because this method only applies to JsonImage response data

        // collect base64 png images into array
        let imgs64 = [];
        data.forecasts[model].members[model][par].forEach(x => {
            x = 'data:image/png;base64,' + x;
            imgs64.push(x);
        });
        console.log(this._class_name+":got", imgs64.length, "images");
        this.images = imgs64;

        // collect time stamps
        let times = data.forecasts[model].times;
        let timesJS = [];
        times.forEach(t => {
            timesJS.push(new Date(t*1000.0));
        });

        // store JS time stamps
        this.times = times;
        this.timesJS = timesJS;
        this.idx = 0; // first image displayed

        // create overlay with first image
        let im64 = imgs64[this.idx];
        let boundsll = data.forecasts[model].gridll;
        let imageBounds = [[boundsll[1], boundsll[0]], [boundsll[3], boundsll[2]]];
        let olOpts = {}
        if(opts.pane) olOpts.pane = opts.pane;
        let ol = L.imageOverlay(
            im64,
            imageBounds,
            olOpts)
        
        // store the overlay
        this.imageOverlay = ol;

        // if map provided add ol to map
        this.maps = [];
        if(map) {
            ol.addTo(map);
            // keep track of attached maps...
            this.maps.push(map);
        }

        // create time step lookup,
        let timeLookup = SeaMon.lookup.timeStepLookup(times);
        this._timeLookup = timeLookup;
    }
    length() {
        return this.times.length;
    }
    getBounds() {
        return this.imageOverlay.getBounds();
    }
    setIndex(idx) {
        if(idx > this.length()) {
            console.error(this._class_name + `:cant show image, index ${idx} exceeds length of forecast`);
            return;
        }
        this.imageOverlay.setUrl(this.images[idx]);
        this.idx = idx;
    }
    lookup(t){
        return this._timeLookup.nearest({x:t}, 1)[0][0].idx;
    }
    setTime(t) {
        let idx = this.lookup(t);
        this.setIndex(idx);
    }
    time() { return this.times[this.idx]; }
    timeJS() { return this.timesJS[this.idx]; }
    next() {
        this.idx = (this.idx + 1)%this.length();
        this.setIndex(this.idx);
    }
    addTo(map) {
        ol.addTo(map);
        // keep track of attached maps...
        this.maps.push(map);
    }
    remove(map=null) {
        if(map) {
            map.removeLayer(this.imageOverlay);
            this.maps = this.maps.filter(item => item !== map);
            return;
        }
        // else remove from all maps in the maps array,
        this.maps.forEach(x => {
            x.removeLayer(this.imageOverlay);
        });
        this.maps = [];
    }
}

/* takes a jsonimage (base64 encoded) map response from SeaMon-API
   and creates a playable leaflet imageOverlay on the map,
   returning a play controller to step through the images on 
   the map overlay */
function imagePlayer(data, model, par, map, opts={}) {
    let funcName = "imagePlayer:";

    // play control object
    let player = new ImagePlayer(data, model, par, map, opts);
    return player;
}

export default imagePlayer;