import L from 'leaflet';

function waypoints(data, map, svgIcon=null) {
    var markersGroup = new L.featureGroup();
  
    // set svg icon
    let icon = `<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24">
                <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"/><path d="M0 0h24v24H0z" fill="none"/>
                </svg>`;
    if(svgIcon) icon = svgIcon;
    var PosIcon = L.icon({
        iconUrl: 'data:image/svg+xml;base64,' + btoa(icon),
        iconSize: [34, 34], // size of the icon
        iconAnchor: [17,30],
        popupAnchor: [0, -20]
    });
  
    // collect markers
    var waypointMarkers = [];
    for (var route in data.waypts) {
      data.waypts[route].forEach( wp => {
        // markers have labels...
        if (wp.label != "") {
          var n = waypointMarkers.length;
          waypointMarkers[n] = {'lat': wp.lat, 'lon':wp.lon, 'label': wp.label}
  
          // add to marker group
          let pop = L.popup({closeButton: false,
                            className: 'loclabel'}).setContent(wp.label);
          L.marker([wp.lat, wp.lon], {icon:PosIcon}).bindPopup(pop).addTo(markersGroup);
        }
      });
    }

    if(map) markersGroup.addTo(map);
  
    return markersGroup;
  }

  export default waypoints;
  
  