import L from 'leaflet';
import 'leaflet-arc';
import { generateCritical, stylesState } from '../../styles/styles';

// plots path based on the forecast lat lon values.
// this path only extends as far as the forecast reaches,
// but is one2one with the forecast data
function path(data, model, map, opts={}) {

    // collect forecast points
    let lats = data.forecasts[model].lats;
    let lons = data.forecasts[model].lons;

    if(!('pane' in opts)) opts.pane = null;

    // inject critical values and style coloring
    // for lines if not provided with opts...
    if(!('critical' in opts)) {
      let { critical, encs } = generateCritical(data);
      opts['critical'] = critical;
      opts['criticalStyle'] = stylesState.criticalStyle;
    }

    // line between points drawn here
    let fg = __line_feature(lats, lons, opts);
  
    // add to map if not null
    if(map) fg.addTo(map);
    
    return fg;
  }
  
  // Great circle arc midpoint calculation
  function __midpoint(p1, p2) {
    let deg2rad = Math.PI / 180.0;
    let rad2deg = 1.0/deg2rad;
    let lon1 = p1.lng*deg2rad;
    let lat1 = p1.lat*deg2rad;
    let lon2 = p2.lng*deg2rad;
    let lat2 = p2.lat*deg2rad;
    let dlon = lon2 - lon1;
  
    let Bx = Math.cos(lat2) * Math.cos(dlon);
    let By = Math.cos(lat2) * Math.sin(dlon);
    let lat = Math.atan2( Math.sin(lat1) + Math.sin(lat2),
                           Math.sqrt( (Math.cos(lat1)+Bx)*(Math.cos(lat1)+Bx) + By*By)  );
    let lon = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);
    return new L.LatLng(lat*rad2deg, lon*rad2deg);
  }
  
  function __line_feature(lats, lons, opts) {
    let fg = L.featureGroup();
    let bgLines = [];
    let fgLines = [];
    for(let i=1; i<lats.length; i++) {
      let p0 = new L.LatLng(lats[i-1], lons[i-1]);
      let p1 = new L.LatLng(lats[i], lons[i]);
  
      // default style
      let s0 = {color: 'black', weight:3};
      let s1 = {color: 'black', weight:3};
      // critical array override
      if('critical' in opts) {
        let c0 = opts.critical[i-1];
        let c1 = opts.critical[i];
        if(c0 in opts.criticalStyle)
          s0 = Object.assign(s0, opts.criticalStyle[c0]);
        if(c1 in opts.criticalStyle)
          s1 = Object.assign(s1, opts.criticalStyle[c1]);
      }
  
      // has bgLine in opts,
      let hasBgLine = false;
      if('bgLine' in opts) {
        hasBgLine = true;
        opts.bgLine.className = 'seamon-path-element-backdrop';
      }
      // additional line opts,
      let eOpts = {};
      if(opts.pane) eOpts.pane = opts.pane;

      // only add points that are different
      if( p0.lat!==p1.lat || p0.lng !== p1.lng) {
        // background line
        if(hasBgLine) {
          bgLines.push( L.Polyline.Arc(p0, p1, {...opts.bgLine, ...eOpts}) );
        }
        // foreground line
        //// split into two segments to interpolate between colors,
        //// only if color code is different between the points...
        if(s0.color !== s1.color) {
          let pMid = __midpoint(p0, p1);
          // first half
          let line1 = L.Polyline.Arc(p0, pMid, {
            color: s0.color,
            weight: s0.weight,
            opacity: 1,
            className: 'seamon-path-element',
            dataIndex: i-1,
            ...eOpts
          }).on('mouseover', (e) => {
            if('hover' in opts) {
              let di = e.target.options.dataIndex;
              opts.hover(di);
            }});
          fgLines.push(line1);
          // second half
          let line2 = L.Polyline.Arc(pMid, p1, {
            color: s1.color,
            weight: s1.weight,
            opacity: 1,
            className: 'seamon-path-element',
            dataIndex: i, ...eOpts
          }).on('mouseover', (e) => {
            if('hover' in opts) {
              let di = e.target.options.dataIndex;
              opts.hover(di);
            }});
          fgLines.push(line2);
        } else {
          let line = L.Polyline.Arc(p0, p1, {
            color: s0.color,
            weight: s0.weight,
            opacity: 1,
            className: 'seamon-path-element',
            dataIndex: i-1, ...eOpts
          }).on('mouseover', (e) => {
            if('hover' in opts) {
              let di = e.target.options.dataIndex;
              opts.hover(di);
            }});
          fgLines.push(line);
        }
      }
    }
    // insert segments in correct draw order
    bgLines.forEach(x=>{x.addTo(fg);})
    fgLines.forEach(x=>{x.addTo(fg);})
    return fg;
  }
  
export default path;