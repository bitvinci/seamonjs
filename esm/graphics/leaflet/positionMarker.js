function positionMarker(map, opts={}) {
    
    // set options from opts
    let popup = false;
    if(popup in opts) popup = opts.popup;
    if(!opts.fill) opts.fill = "black";

    let icon = `<svg xmlns="http://www.w3.org/2000/svg"
                     width="24" height="24" viewBox="0 0 24 24"
                     fill="${opts.fill}">
                <path d="M18.92 6.01C18.72 5.42 18.16 5 17.5 5h-11c-.66 0-1.21.42-1.42 1.01L3 12v8c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-1h12v1c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-8l-2.08-5.99zM6.5 16c-.83 0-1.5-.67-1.5-1.5S5.67 13 6.5 13s1.5.67 1.5 1.5S7.33 16 6.5 16zm11 0c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zM5 11l1.5-4.5h11L19 11H5z"/><path d="M0 0h24v24H0z" fill="none"/></svg>`;
    if(opts.svgIcon) icon = opts.svgIcon;
    // opts alt property on leaflet markers best way to
    // allow for css selection img[alt=...]
    if(!opts.alt) opts.alt = "";
  
    let eOpts = {};
    if('pane' in opts) eOpts.pane = opts.pane;

    // add position marker to map
    let size = [35, 35];
    if(opts.size) size = opts.size;
    let PosIcon = L.icon({
        iconUrl: 'data:image/svg+xml;base64,' + btoa(icon),
        iconSize: size
    });
    
    let pos_marker = L.marker(new L.LatLng(0, 0), {icon:PosIcon, alt: opts.alt, ...eOpts});
    let pop = null;
    if( popup ) {
      pop = L.popup({closeButton: false,
                     className: 'poslabel'});
      pos_marker.bindPopup(pop, {autoClose: false,
                                 closeOnClick: false,
                                 // autopan false necessary to get everythin centered on map
                                 // unintuitive feature...
                                 autoPan: false});
    }
    if(map) pos_marker.addTo(map).openPopup();
  
    // update marker method...
    let setPositionMarker = (latlon, content=null, panTo=false) => {
      let newLatLng = new L.LatLng(latlon[0], latlon[1]);
      pos_marker.setLatLng(newLatLng);
      // todo update content in popup of marker...
      // i.e. do something with pop var here....
      if(panTo) map.panTo(newLatLng,{'animate': true,
                                     'duration': 0.25});
    };
  
    return {marker: pos_marker, setPositionMarker}; 
  }

  export default positionMarker;