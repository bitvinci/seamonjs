//import * as THREE from 'three';
import { WebGLRenderer, OrthographicCamera, Scene } from 'three';
import { BoxGeometry, SphereGeometry, MeshBasicMaterial, Mesh } from 'three';
import { LineBasicMaterial, BufferGeometry, Vector3, Line } from 'three';
import { TextureLoader, Box3 } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'; 
import getCoastlines from './getCoastlines';

const loader = new TextureLoader();

class Map {
    width() {
        return this.element.offsetWidth;
    }
    height() {
        return this.element.offsetHeight;
    }
    constructor() {
        this.element = document.getElementById("webgl-div");
        let width = this.element.offsetWidth;
        let height = this.element.offsetHeight;
        this.scene = new Scene();
    
        // set up an orthographic camera
        let aspect = width/height;
        this.camera = new OrthographicCamera( -aspect*1, aspect*1, 1, -1, 0.01, 100);
    
        this.renderer = new WebGLRenderer({antialias:true});
        this.renderer.setSize(width, height);
    
        this.element.appendChild( this.renderer.domElement );
    
        // camera controls...
        this.controls = new OrbitControls( this.camera, this.renderer.domElement );
        this.controls.enablePan = false;
        this.rotateSpeed = 0.3;
        this.controls.rotateSpeed = this.rotateSpeed;
        this.camera.position.z = 10;
    
        // lookat objects are for objects that should orient
        // towards the camera - e.g. text.
        this.lookAtObjects = [];
        this.render = () => {
            this.renderer.render( this.scene, this.camera );
            this.lookAtObjects.forEach(ob => {
                ob.lookAt(this.camera.position.x, this.camera.position.y, this.camera.position.z);
            });
            let z = this.camera.zoom;
            this.controls.rotateSpeed = this.rotateSpeed/z;
        };
    
        /////////////////////
        add_sphere(this.scene, 0.995);
        //add_image(this.scene, 0.998);
        add_graticules(this.scene);
        /////////////////////
    
        // Only render scene on camera viw change...
        // because scene is static
        // for animations need activate animation frame code...
        this.controls.addEventListener('change', this.render);
        this.render(); // initial render...
    
        // on window resize, must reset and redraw...
        this.resize = () => {
            console.log("resizing webgl map");

            let width = this.width();
            let height = this.height();
            let aspect = width/height;
            this.renderer.setSize(width, height);
            // enforce the orthographic camera aspect ratio
            this.camera.left = this.camera.bottom*aspect;
            this.camera.right = this.camera.top*aspect;
            this.camera.updateProjectionMatrix();
            this.render();
        };
        window.addEventListener('resize', this.resize);

    }

    /* fetches and adds coastline data */
    coastlines() {
        getCoastlines()
        .then((tj) => {
            console.log("hello");
            add_topojson_lines(this.scene, tj);
            this.render();
        });
    }

    setCameraPosition(lat, lon) {
        let u = lon*Math.PI/180.0;
        let v = lat*Math.PI/180.0;
        //let pi2 = Math.PI/2.0;

        let r = 10.0;
        let x = Math.cos(v)*Math.sin(u);
        let y = Math.sin(v);
        let z = Math.cos(v)*Math.cos(u);

        console.log(this.camera.position);
        this.camera.position.set(r*x,r*y,r*z);
        console.log(this.camera.position);

        this.controls.update();
        console.log(this.camera.position);
    }

    imageTile(img, bbox) {

        let res = add_image(this.scene, 0.999, img, bbox);
        let tile = res.object;
        let avLat = res.avLat;
        let avLon = res.avLon;
        this.setCameraPosition(avLat, avLon);

        // fit fov/zoom height to this tile...
        let dLat = res.dLat;
        let gH = Math.sin(dLat*Math.PI/180.0);
        let zoom = 2.0/gH;
        this.camera.zoom = zoom;
        this.camera.updateProjectionMatrix();

        this.render();
        // quick fix (texture loader async problem)...
        setTimeout(()=>{
            this.render();
        }, 50);
    }
}

function map() {
    return new Map();
}

export default map;

// lat long to xyz (length scale 1 == earth radius)
function ll2xyz(lonlat) {
    var lonr = lonlat[0]*Math.PI/180.0;
    var latr = lonlat[1]*Math.PI/180.0;
    return [Math.cos(latr)*Math.sin(lonr), Math.sin(latr), Math.cos(latr)*Math.cos(lonr)];
}

// lat long to xyz (height as fraction of earth radius)
function ll2xyz_height(lonlat, height) {
    var lonr = lonlat[0]*Math.PI/180.0;
    var latr = lonlat[1]*Math.PI/180.0;
    return [height*Math.cos(latr)*Math.sin(lonr), height*Math.sin(latr), height*Math.cos(latr)*Math.cos(lonr)];
}

// add polyline to map
function add_polyline(scene, coords, material, closed=false) {
    let points = [];
    for (var i = 0; i < coords.length; i++) {
        const xyz = ll2xyz(coords[i]); // note the topojson coords are (lon,lat) order
        points.push(new Vector3(...xyz));
    }
    // close the line loop if closed==true by appending the first vertice
    if (closed) points.push(points[0]);

    let geometry = new BufferGeometry().setFromPoints( points );

    let poly_line = new Line( geometry, material );
    scene.add( poly_line );
}

// Add great circle path between points on sphere, controlled by definition in km
// This method should be used when plotting lines over long distances on the sphere.  
function add_great_circle_polyline(coords, material, closed=false, definition=500.0) {
}

// add base sphere
function add_sphere(scene, rad) {
    let geometry = new SphereGeometry( rad, 64, 64 );
    let material = new MeshBasicMaterial( {color: 0x002222} );
    let sphere = new Mesh( geometry, material );
    scene.add( sphere );
}

function add_image(scene, rad, img, bbox) {
    // create mesh tile for image...
    let lon0 = bbox[0];
    let dlon = bbox[2] - bbox[0];
    let lat0 = bbox[1];
    let dlat = bbox[1] - bbox[3];

    let u = (lon0+90.0)/180.0*Math.PI;
    let v = (90.0-lat0)/180.0*Math.PI;
    let du = dlon/180.0*Math.PI;
    let dv = dlat/180.0*Math.PI;
    
    let geometry = new SphereGeometry(rad, 64, 64, 
        u, du, v, dv);

    let material = new MeshBasicMaterial({
        /*color: 0xff0000*/
        map: loader.load(img)
    });
    let tile = new Mesh( geometry, material );

    scene.add( tile );

    return {object:tile, avLat: lat0-dlat/2.0, avLon: lon0+dlon/2.0,
            dLat: dlat, dLon: dlon};
}

// add graticules
function add_parallel(scene, deg, material) {
    var n = 60;
    var coords = [];
    for(var i=0; i<n; i++) {
        var lat = deg;
        var lon = i*360.0/n;
        coords[i] = [lon,lat];
    }
    add_polyline(scene, coords, material, true);
}

function add_meridian(scene, deg, material, min_lat=-80.0, max_lat=80.0) {
    var n = 60;
    var coords = [];
    for(var i=0; i<=n; i++) {
        var lon = deg;
        var lat = min_lat + i*2.0*max_lat/n;
        coords[i] = [lon,lat];
    }
    add_polyline(scene, coords, material, true);
}

function add_graticules(scene) {
    var material = new LineBasicMaterial( {color: 0x505050, linewidth:0.5} );
    for (var lat=-80; lat<=80; lat+=10)
        add_parallel(scene, lat, material);
    for (var lon=0; lon<360; lon+=10)
        if (lon%90 == 0)
        add_meridian(scene, lon, material, -90, 90);
        else
        add_meridian(scene, lon, material,-80,80);
}

function add_topojson_lines(scene, tj) {
    let material = new LineBasicMaterial( {color: 0x00ffff, linewidth:0.73} );
    tj.features.forEach(f => {
        let coords = f.geometry.coordinates;
        add_polyline(scene, coords, material, false);
    });
}

// Load coastlines from geojson
/*function add_coastlines() {
queue()
    .defer(d3.json, "/static/world-110m.json")
    .await( function (e, world) {
                console.log("loading coastlines");
                var material = new THREE.LineBasicMaterial( {color: 0x00ffff, linewidth:0.73} );
                var land_data = topojson.object(world, world.objects.land);
                const coords = land_data.coordinates;

                for (var i=0; i<coords.length; i++)
                    for (var k=0; k<coords[i].length; k++)
                    add_polyline(land_data.coordinates[i][k], material);

                // update land/coastline data
                //svg.select("path.land")
                //    .datum(topojson.object(world, world.objects.land))
                //   .attr("d", path);

                // update countries outlines
                //svg.select("g.countries")
                //    .selectAll("path")
                //    .data(topojson.object(world, world.objects.countries).geometries)
                //    .attr("d", path);
            });
}

// draw waypoint locations and waypoint paths
// waypoint locations
waypts = [
% for _, routeData in data['waypts'].iteritems():
% for pt in routeData:
    % if pt['label'] != "":
    { label:"${pt['label']}", lat:${pt['lon']}, lon:${pt['lat']} },
    % endif
% endfor
% endfor
];

wayptsPaths = [
% for _, routeData in data['waypts'].iteritems():
[
% for pt in routeData:
    [${pt['lon']}, ${pt['lat']}],
% endfor
],
% endfor
];

function add_waypoint_location(lat, lon, label) {
// location point
var geometry = new THREE.SphereGeometry( 0.01, 8, 8 );
var material = new THREE.MeshBasicMaterial( {color: 0xffffff} );
var marker = new THREE.Mesh( geometry, material );
xyz = ll2xyz_height([lon,lat],1.0);
marker.position.set(...xyz);
scene.add( marker );

// location label
var loader = new THREE.FontLoader();
loader.load( '/static/helvetiker_regular.typeface.json', function(font) {
    var geometry = new THREE.TextGeometry( label, {
    font: font,
    size: 0.05,
    height: 0.0,
    //curveSegments: 12,
    //bevelEnabled: true,
    //bevelThickness: 10,
    //bevelSize: 8,
    //bevelSegments: 5
    });
    var txt = new THREE.Mesh( geometry, material );
    xyz = ll2xyz_height([lon,lat],0.05);
    txt.position.set(...xyz);
    marker.add(txt);
    lookAtObjects.push(txt);
});

return marker;
}

function add_ship() {
if ( waypts.length>0 ) ship = add_waypoint_location(waypts[0].lon, waypts[0].lat, "ship");
else ship = add_waypoint_location(0.0, 0.0, "ship");
hideShip();
}

function hideShip() {
ship.visible = false;
} 

function updateShipPos(lon, lat) {
ship.visible = true;
var xyz = ll2xyz([lon, lat]);
ship.position.set(...xyz);
}

// set up scene objects
add_sphere(0.995);
add_graticules();
add_coastlines();

// waypoint locations
for (var i=0; i<waypts.length; i++) add_waypoint_location(waypts[i].lon, waypts[i].lat, waypts[i].label);

// waypoint paths
var material = new THREE.LineBasicMaterial( {color: 0xFF0000} );
for (var i=0; i<wayptsPaths.length; i++) {
const coords = wayptsPaths[i];
var gc_coords = great_circle_path(coords);
add_polyline(gc_coords, material);
}

// ship marker
add_ship();




//========== haversine math ==============
r_earth = 6371.0; // in km

function radians(angle) {
return angle*Math.PI/180.0;
}

function degrees(angle) {
return angle*180.0/Math.PI;
}

// return angular distance between points on sphere in radians
function haversine_angular_distance(lonlat1, lonlat2) {
var latRad1 = radians(lonlat1[1]);
var latRad2 = radians(lonlat2[1]);
var DlatRad = latRad2 - latRad1;
var DlonRad = radians(lonlat2[0] - lonlat1[0]);
var a = Math.pow(Math.sin(DlatRad/2),2) + Math.cos(latRad1)*Math.cos(latRad2) * Math.pow(Math.sin(DlonRad/2),2);
var one_minus_a = 1-a;
if (one_minus_a < 0.0) one_minus_a = 0.0; // avoiding rounding error
return 2*Math.atan2(Math.sqrt(a), Math.sqrt(one_minus_a));
}

// returns great circle distance in km between points 
function haversine_distance(lonlat1, lonlat2) {
return r_earth*haversine_angular_distance(lonlat1, lonlat2);
}

// Intermetiate point a fraction f along great circle path between points
function intermediate_point(lonlat1, lonlat2, f) {
var latRad1 = radians(lonlat1[1]);
var latRad2 = radians(lonlat2[1]);
var lonRad1 = radians(lonlat1[0]);
var lonRad2 = radians(lonlat2[0]);
var DlatRad = latRad2 - latRad1;
var DlonRad = lonRad2 - lonRad1;
var angDist = haversine_angular_distance(lonlat1, lonlat2);
if (angDist == 0.0) angDist = 1.0e-7; // make smaller than a meter on the sphere, but never zero

var a = Math.sin((1-f)*angDist) / Math.sin(angDist);
var b = Math.sin(f*angDist) / Math.sin(angDist);

var x = a*Math.cos(latRad1)*Math.cos(lonRad1) + b*Math.cos(latRad2)*Math.cos(lonRad2);
var y = a*Math.cos(latRad1)*Math.sin(lonRad1) + b*Math.cos(latRad2)*Math.sin(lonRad2);
var z = a*Math.sin(latRad1) + b*Math.sin(latRad2);

var latRadPt = Math.atan2(z, Math.sqrt(x*x+y*y));
var lonRadPt = Math.atan2(y, x);
return [degrees(lonRadPt), degrees(latRadPt)];
}

function great_circle_path(coords, definition=500.0) {
// loop over coordinate pairs
var gc_coords = [];

for (var i=0; i<coords.length-1; i++) {
    var a = coords[i];
    var b = coords[i+1];
    let dist = haversine_distance(a,b);

    // add starting point
    gc_coords.push(a);

    // add intermediate points if needed (up to but not including destination point)
    if (dist > definition) {
        let steps = parseInt(dist/definition);
        for (k=1; k<steps; k++) {
        let f = k * definition/dist;
        gc_coords.push( intermediate_point(a, b, f) );
        } 
    }
}
//finally add the final destination point
gc_coords.push(b);
return gc_coords;
}*/

//========================================