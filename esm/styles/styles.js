// temp translate T() function
function T(s) {
    return s;
}

function getStyleDef(parType) {
  if(parType in styleDefs) {
    return styleDefs[parType];
  }
  return styleDefs['default'];
}

function generateStyle(value, parType) {
  let def = getStyleDef(parType);
  return styleCoder(value, def);
}

/* generates an array of warning levels and
weather encounters along path using all forecast 
data collectively */
function generateCritical(data) {
    let modelName = Object.keys(data.forecasts)[0];
    let fcData = data.forecasts[modelName].members[modelName];
    let times = data.forecasts[modelName].times;
    let dists = data.forecasts[modelName].dists;
    let params = Object.keys(fcData).sort();
    
    // collect critical indicator values for path
    let parStylePairs = [];
    params.forEach( p => {
        if(p in styleDefs) {
            parStylePairs.push([fcData[p], styleDefs[p]]);
        }
    });
    let coll = collectCritical(parStylePairs, times=times, dists=dists);
    let critical = coll.critical;
    let encs = coll.encounters;

    return { critical, encs };
}

// HSL color scheme to maintain
// uniform contrast and brightness of colors
var colS = 100;
var colL = 70;
function hslStr(h,s,l) {
  return `hsl(${h},${s}%,${l}%)`;
}

// critical line and text styles lookup
// this is used in plotting path, lines and
// param hilights -
// set all lines to fixed width, because using
// background line in plotPath
const criticalStyle = {
  0: {color:'#0a0', weight:6, style: ''},
  1: {color: '#dd0', weight: 6, style: 'font-weight:bold;'},
  2: {color: '#f33', weight: 6, style: 'font-weight:bold;'},
  3: {color: '#e0e', weight: 6, style: 'font-weight:bold;'}};

// data color code and style rules
const windStyleDef = [
  {
    lte: 12,
    critical: 0,
  },
  {
    gt: 12,
    lte: 20,
    critical: 1,
    message: "moderate winds"},
  {
    gt: 20,
    lte: 33,
    critical: 2,
    message: "strong winds"
  },
  {
    gt: 33,
    critical: 3,
    message: "hurricane winds"
  }];

const gustStyleDef = [
  {
    lte: 16,
    critical: 0
  },
  {
    gt: 16,
    lte: 26.6,
    critical: 1,
    message: "moderate gusts"},
  {
    gt: 26.6,
    lte: 43.9,
    critical: 2,
    message: "strong gusts"
  },
  {
    gt: 43.9,
    critical: 3,
    message: T("extreme gusts")
  }];

const tempStyleDef = [
  {
    lte: -2,
    critical: 0,
  },
  {
    gt: -2,
    lte: 2,
    critical: 0
  },
  {
    gt: 2,
    critical: 0
  }];

const precrStyleDef = [
  {
    lte: 3,
    critical: 0
  },
  {
    gt:3,
    lte: 5,
    critical: 1,
    message: T("medium precipitation")},
  {
    gt: 5,
    critical: 2,
    message: T("heavy precipitation")
  }];

const rainrStyleDef = [
  {
    lte: 3,
    critical: 0
  },
  {
    gt:3,
    lte: 6,
    critical: 1,
    message: T("rainfall")},
  {
    gt: 6,
    critical: 2,
    message: T("heavy rainfall")
  }];

const snowrStyleDef = [
  {
    lte: 0.5,
    critical: 0
  },
  {
    gt:0.5,
    lte: 3,
    critical: 1,
    message: T("snowfall")},
  {
    gt: 3,
    critical: 2,
    message: T("heavy snowfall")
  }];

const snowrProxStyleDef = [
  {
    lte: 0.5,
    critical: 0,
  },
  {
    gt:0.5,
    lte: 3,
    critical: 1,
    message: T("snowfall in proximity")
  },
  {
    gt: 3,
    critical: 2,
    message: T("heavy snowfall in proximity")
  }];

const visStyleDef = [
  {
    gt: 500,
    critical: 0,
  },
  {
    gt:100,
    lte: 500,
    critical: 1,
    message: T("reduced visibility")},
  {
    lte: 100,
    critical: 2,
    message: T("low visibility")
  }];

// build road style def from combinatorics,
// of ibtwise boolean conditions
const roadStyleDef = function() {
  let def = [{lte: 0.5, critical: 0, code: ''}];
  let roadConds = [{msg:T("wet road"), critical: 0, code: 'W'},
                   {msg:T("snow on road"), critical: 1, code: 'S'},
                   {msg:T("icing risk"), critical: 1, code: 'I'},
                   {msg:T("strong icing risk"), critical: 2, code: 'I'}];

  // loop through all road conditions
  // binary flags 0001 to 1111...
  for(let i=1; i<Math.pow(2, roadConds.length); i++) {
    let flags = i.toString(2).split('').reverse();
    let minVal = i-0.5;
    let maxVal = i+0.5;
    let msg = '';
    let critical = 0;
    let code = '';
    flags.forEach((x,k) => {
      if(x == '1') {
        // prioritize msg from higher flag positions...
        msg = roadConds[k].msg + ', ' + msg;
        if(critical < roadConds[k].critical) critical = roadConds[k].critical;
        code += roadConds[k].code;
      }
    });
    msg = msg.slice(0, -2); // strip last ', '
    // combine overlapping messages,
    msg = msg.replace("strong icing risk, icing risk", "strong icing risk");

    // insert this style def range...
    def.push(
      {
        gt: minVal,
        lte: maxVal,
        critical: critical,
        message: msg,
        code: code,
      }
    );
  }
  // return the constructed def
  return def;
}();

const defaultStyleDef = [
  {
    gt: -999999,
    critical: 0,
  }];

// parameters styledef lookup
var styleDefs = {
    default: defaultStyleDef,
    wind: windStyleDef,
    gust: gustStyleDef,
    temp: tempStyleDef,
    precr: precrStyleDef,
    rainr: rainrStyleDef,
    'rainr-10km-max': rainrStyleDef,
    snowr: snowrStyleDef,
    'snowr-10km-max': snowrProxStyleDef,
    vis: visStyleDef,
    road: roadStyleDef
  }

// Styles state object...
let stylesState = {
    criticalStyle,
    styleDefs,
};
  
// collect critical values from multiple [data def] pairs
// critical is an attribute that if provided in def is collected
// and largest one of all pairs is returned
// data must all be same length
// e.g. used for coloring/styling route path on a map

// collect message points for path, where change
// in critical def occurs -- i.e. at beginning of
// each segment
function collectCritical(pairs, times=null, dists=null) {
  // buffer for critical  messages along route
  // only at encounters with new conditions
  let encs = [];
  // create zero initialized array to write over most critical vals
  let N = pairs[0][0].length;
  let crit = Array.from({length: N}, ()=>(0));
  // create array of all conditions along path
  let cond = Array.from({length: N}, ()=>({critical:0, msgs:[]}));

  let prevEnc = null;
  for(let k=0; k<pairs.length; k++) {
    let prev_c = 0;
    let def = pairs[k][1];

    for(let i=0; i<pairs[0][0].length; i++) {
      let val = pairs[k][0][i];
      let stl = styleCoder(val,def);
      let c = stl.critical;
      let m = stl.message;

      // register this condition along path
      // if of any critical level above 0...
      if(c > 0) {
        if(c > cond[i].critical) {
          cond[i].critical = c;
          // also register the critical array
          crit[i] = c;
        }
        cond[i].msgs.push(m);
      }

      // pick out level changes along path - weather encounters...
      if( c != prev_c && c > 0) {
        // register this new encounter at index i
        let enc = {'msgs':[m], 'critical':c, 'index': i};
        if(times) enc['time'] = times[i];
        if(dists) enc['dist'] = dists[i];
        encs.push(enc);
      }
      prev_c = c;
    }
  }

  // create index sorted and merged weather encounters,
  let mEncs = [];
  // sort the original encounters by index
  encs.sort((a,b) => (a.index > b.index ) ? 1: -1);

  // insert or combine as appropriate
  encs.forEach(x => {
    let prev = mEncs[mEncs.length-1];
    if(prev) {
      if(prev.index == x.index) {
        // combine...
        if(x.critical > prev.critical) prev.critical = x.critical;
        prev.msgs.push( x.msgs[0] );
      } else {
        // add new one ...
        mEncs.push(x);
      }
    } else {
      mEncs.push(x);
    }
  });
  // sort all the messages for consistency
  mEncs.forEach(x => {
    x.msgs.sort();
  });

  return {'critical': crit, 'encounters': mEncs, 'conditions': cond};
}

// color coder based on echarts visualMap pieces definitions
// but includes some extras such as custom style and critical indicator
function styleCoder(val, def, override={}) {
  for(let i=0; i<def.length; i++) {
    let d = def[i];
    let gtBool = "gt" in d;
    let lteBool = "lte" in d;
    let eq = "eq" in d;
    let critical = d.critical;
    // add message if provided
    let msg = '';
    if("message" in d) msg = d.message;
    // add code if provided
    let code = '';
    if("code" in d) code = d.code;

    // pick out criticalStyle codes for this ciritical code
    let cs = criticalStyle[critical];
    let color = cs.color;
    let style = cs.style;

    // overrides if provided
    if(override[color]) color = override[color];
    if(override[style]) style = override[style];

    // filter steps
    if (eq) {
      // check eq result first, as its priority...
      if(val == d.eq) return {color: color, style: style, critical: critical, message: msg, code: code};
    } else if(gtBool && lteBool) { // gt-lte range
      if(val>d.gt && val <= d.lte) return {color: color, style: style, critical: critical, message: msg, code: code};
    } else if (gtBool && !lteBool) { // only gt
      if(val>d.gt) return {color: color, style: style, critical: critical, message: msg, code: code};
    } else if (!gtBool && lteBool) { // only lte
      if(val <= d.lte) return {color: color, style: style, critical: critical, message: msg, code: code};
    } else {
      throw "code gt and lte missing from def";
    }
  }
  // if style def incorrectly defined we will end up here
  // and value will pass through filter, throw error...
  throw "style coding failed to catch value";
}

export { generateStyle, generateCritical, criticalStyle, stylesState };