import { sprintf } from 'sprintf-js';
import { generateStyle } from './styles';

function round(val,prec) {
    return Math.round(val/prec)*prec;
}

const formatters = {
    default: (val) => {return val.toString();},
    wind: (val) => {
       return sprintf("%.1f", round(val,0.1)); 
    },
    gust: (val) => {
        return sprintf("%.1f", round(val,0.1)); 
    },
    cloud: (val) => {
        return sprintf("%3.0f", round(val,1));
    },
    rain: (val) => {
        return sprintf("%.0f", round(val,1)); 
    },
    snow: (val) => {
        return sprintf("%.1f", round(val,0.1)); 
    },
    temp: (val) => {
        return sprintf("%.1f", round(val,0.1)); 
    },
    precr: (val) => {
        return sprintf("%.1f", round(val,0.1)); 
    },
    rainr: (val) => {
        return sprintf("%.1f", round(val,0.1)); 
    },
    snowr: (val) => {
        return sprintf("%.1f", round(val,0.1)); 
    },
    'rainr-10km-max': (val) => {
        return sprintf("%.1f", round(val,0.1)); 
    },
    'snowr-10km-max': (val) => {
        return sprintf("%.1f", round(val,0.1)); 
    },
    vis: (val) => {
        if(val>999) { return ">999"; }
        return sprintf("%.0f", round(val,1)); 
    },
    road: (val) => {
        return generateStyle(val, 'road').code;
    },
};

function formatValue(value, parType) {
    if(parType in formatters) {
        return formatters[parType](value);
    }
    return formatters.default(value);
}

export { formatValue };