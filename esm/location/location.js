import api_error from '../errors';

/* location forecast query */
function location(pos, start=null, stop=null, params, opts={}) {
    // force default opts
    if(!opts.server) opts.server = 'https://api.seamon.io';
    if(!opts.token) opts.token = '';
    if(!opts.save) opts.save = false;
    if(!opts.allowMissingPar) opts.allowMissingPar = false;
    if(!opts.limit) opts.limit = null;

    // compose URL query - -
    let url = opts.server + '/location?';

    // add lat lon pos arg
    if(Array.isArray(pos)) {
        url += `pos=(${pos[0]};${pos[1]})`;
    } else {
        url += `pos=${pos}`;
    }
    // start/stop
    if(start) url += `&start=${start}`;
    else url += `&start=now`;
    if(stop) url += `&stop=${stop}`;

    // add forecast parameter selection
    url += "&params=";
    params.forEach(function(x) {
        url += `${x},`;
    });

    // limit time steps,
    if(opts.limit) url += `&limit=${opts.limit}`;

    // add token
    url += "&token=" + opts.token;

    // add missing pars allowed, if so,
    if(opts.allowMissingPar) url += "&allow_missing_par"

    console.log("query: " + url);

    // query seamon
    return fetch(url)
    .catch(e => {
    console.error(`fetch had error in ${fcmethod}`, e);
    return Promise.reject(new Error("failed to reach seamon-api service - check internet connection"));
    })
    .then(resp => {
    // check here for unexpected HTTP errors . e.g. 502
    if ([502, 504].includes(resp.status)) {
    return Promise.reject(new Error(`${resp.status} gateway error - service may be down - admin will be notified`));
    }
    // all ok so pass on...
    return resp;
    })
    .then(resp => resp.json()) // remember... .then() is sugar for await...
    .then(json => {
    if('error' in json) {
    // so this is an API error, so
    // pass rejected promise with API error
    return Promise.reject(api_error(json));
    }
    return json;
    });
}

export default location;