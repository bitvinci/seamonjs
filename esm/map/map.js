import api_error from '../errors';

/* basic forecast query */
function map(
        gridll,
        start=null,
        stop=null,
        params=null,
        opts={}) {
    // force default opts
    if(!opts.server) opts.server = 'https://api.seamon.io';
    if(!opts.token) opts.token = '';
    if(!opts.save) opts.save = false;
    if(!opts.allowMissingPar) opts.allowMissingPar = false;
    if(!opts.proj4) opts.proj4 = null;
    if(!opts.view) opts.view = 'json';
    if(!opts.scales) opts.scales = null;
    if(!opts.latlons) opts.latlons = null;
    if(!opts.scaleInterpolation) opts.scaleInterpolation='linear';
    if(!opts.antialias) opts.antialias=0;

    // compose URL query
    let url = opts.server + '/map?gridll=';

    let gdef = '('
    gridll.forEach(function(x){
        gdef += x + ",";
    });
    gdef = gdef.slice(0,-1);
    gdef += ')';
    url += gdef;

    // add params...
    if(params) {
        let pdef = "&params="
        params.forEach((x)=>{
            pdef += x + ",";
        });
        pdef = pdef.slice(0,-1);
        url += pdef;
    }

    // add proj4
    if(opts.proj4) {
        url += `&proj4=${opts.proj4}`;
    }

    // start/stop
    if(start) {
        url += `&start=${start}`;
    }
    if(stop) {
        url += `&stop=${stop}`;
    }

    // ask for latlong grid values...
    if(opts.latlons) {
        url += "&latlons";
    }

    // add any color scales rendering options...
    if(opts.scales) {
        url += `&scales=${opts.scales}`;
    }
    
    // set scale interpolation option,
    if(opts.scaleInterpolation) {
        url += `&scale_interpolation=${opts.scaleInterpolation}`;
    }

    // set scale interpolation option,
    if('antialias' in opts) {
        url += `&antialias=${opts.antialias}`;
    }
    
    // add the image view...
    url += `&view=${opts.view}`;

    // add token
    if( opts.token ) {
        url += "&token=" + opts.token;
    }

    // add missing pars allowed, if so,
    // if(opts.allowMissingPar) url += "&allow_missing_par"

    console.log("query: " + url);

    // query seamon
    return fetch(url)
    .catch(e => {
        console.error(`fetch had error in ${fcmethod}`, e);
        return Promise.reject(new Error("failed to reach seamon-api service - check internet connection"));
    })
    .then(resp => {
        // check here for unexpected HTTP errors . e.g. 502
        if ([502, 504].includes(resp.status)) {
        return Promise.reject(new Error(`${resp.status} gateway error - service may be down - admin will be notified`));
        }
        // all ok so pass on...
        return resp;
    })
    .then(resp => resp.json()) // remember... .then() is sugar for await...
    .then(json => {
        if('error' in json) {
            // so this is an API error, so
            // pass rejected promise with API error
            return Promise.reject(api_error(json));
        }
        return json;
    });
}

export default map;