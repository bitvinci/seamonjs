import api_error from '../errors';

/* basic forecast query */
function route(waypts, params, solver, opts={}) {
    // force default opts
    if(!opts.server) opts.server = 'https://api.seamon.io';
    if(!opts.token) opts.token = '';
    if(!opts.save) opts.save = false;
    if(!opts.allowMissingPar) opts.allowMissingPar = false;

    // compose URL query - -
    let url = opts.server + `/route?solver=${solver}&waypts=`;
    waypts.forEach(function(x){
    url += `(${x[0]};${x[1]}),`;
    });
    url = url.slice(0,-1);

    // add forecast parameter selection
    url += "&params=";
    params.forEach(function(x){
        url += `${x},`
    });

    // add token
    url += "&token=" + opts.token;

    // add missing pars allowed, if so,
    if(opts.allowMissingPar) url += "&allow_missing_par"


    console.log("query: " + url);

    // query seamon
    return fetch(url)
    .catch(e => {
        console.error(`fetch had error in route.${solver}`, e);
        return Promise.reject(new Error("failed to reach seamon-api service - check internet connection"));
    })
    .then(resp => {
        // check here for unexpected HTTP errors . e.g. 502
        if ([502, 504].includes(resp.status)) {
        return Promise.reject(new Error(`${resp.status} gateway error - service may be down - admin will be notified`));
    }
    // all ok so pass on...
    return resp;
    })
    .then(resp => resp.json()) // remember... .then() is sugar for await...
    .then(json => {
        if('error' in json) {
            // so this is an API error, so
            // pass rejected promise with API error
            return Promise.reject(api_error(json));
        }
        return json;
    });
}

export default route;