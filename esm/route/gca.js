import route from './route';

/* get forecast with drive route method */
function gca(waypts, params, opts={}) {
    return route(waypts, params, "gca", opts)
}

export default gca;