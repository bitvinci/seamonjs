import route from './route';

/* get forecast with drive route method */
function road(waypts, params, opts={}) {
    return route(waypts, params, "road", opts)
}

export default road;