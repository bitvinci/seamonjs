import api_error from '../errors';

/* basic forecast query */
function path(
        waypts,
        start,
        stop,
        params,
        solver,
        opts={}) {
    // force default opts
    if(!opts.server) opts.server = 'https://api.seamon.io';
    if(!opts.token) opts.token = '';
    if(!opts.save) opts.save = false;
    if(!opts.allowMissingPar) opts.allowMissingPar = false;
    if(!opts.view) opts.view = 'json';
    if(!opts.latlons) opts.latlons = null;

    // compose URL query
    let url = opts.server + `/path?solver=${solver}&waypts=`;
    waypts.forEach(function(x){
        url += `(${x}),`;
    });
    url = url.slice(0,-1);
    
    // add params...
    if(params) {
        let pdef = "&params="
        params.forEach((x)=>{
            pdef += x + ",";
        });
        pdef = pdef.slice(0,-1);
        url += pdef;
    }

    // start/stop
    if(start) {
        url += `&start=${start}`;
    }
    if(stop) {
        url += `&stop=${stop}`;
    }

    // ask for latlong grid values...
    if(opts.latlons) {
        url += "&latlons";
    }
        
    // add the view...
    url += `&view=${opts.view}`;

    // add token
    if( opts.token ) {
        url += "&token=" + opts.token;
    }

    // add missing pars allowed, if so,
    if(opts.allowMissingPar) url += "&allow_missing_par"

    console.log("query: " + url);

    // query seamon
    return fetch(url)
    .catch(e => {
        console.error(`fetch had error in path.${solver}`, e);
        return Promise.reject(new Error("failed to reach seamon-api service - check internet connection"));
    })
    .then(resp => {
        // check here for unexpected HTTP errors . e.g. 502
        if ([502, 504].includes(resp.status)) {
        return Promise.reject(new Error(`${resp.status} gateway error - service may be down - admin will be notified`));
        }
        // all ok so pass on...
        return resp;
    })
    .then(resp => resp.json()) // remember... .then() is sugar for await...
    .then(json => {
        if('error' in json) {
            // so this is an API error, so
            // pass rejected promise with API error
            return Promise.reject(api_error(json));
        }
        return json;
    });
}

export default path;