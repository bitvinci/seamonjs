import road from './route/road';
import gca from './route/gca';
import location from './location/location';
import map from './map/map';
import path from './path/path';

import llWaypoints from './graphics/leaflet/waypoints';
import llPath from './graphics/leaflet/path';
import llImagePlayer from './graphics/leaflet/imagePlayer';
import llVectorPlayer from './graphics/leaflet/vectorPlayer';
import llPositionMarker from './graphics/leaflet/positionMarker';
import llWarningMarkers from './graphics/leaflet/warningMarkers';
import llLocationPopup from './graphics/leaflet/locationPopup';
import llCoastlines from './graphics/leaflet/coastlines';
import { generateStyle, generateCritical, criticalStyle } from './styles/styles';
import { getWeatherIcon, weatherIcons } from './styles/svg';
import { formatValue } from './styles/format';
import { timeStepLookup } from './lookup/lookup';

import webgl_map from './graphics/webgl/map.js';

// define namespaces
var SeaMon = {
    route: {road: road,
            gca: gca},
    location: location,
    map: map,
    path: path,
    // fc model query data
    models: {},
    // seamon-api version
    version: null,
    // forecast plot implementations for user convenience
    // e.g. generating forecast graphs and webgis paths
    styles: {
      generateStyle: generateStyle,
      generateCritical: generateCritical,
      criticalStyle: criticalStyle,
      getWeatherIcon: getWeatherIcon,
      getIcon: getWeatherIcon,
      weatherIcons: weatherIcons,
      icons: weatherIcons,
      formatValue: formatValue
    },
    graphics: {
      // webgis plot methods for leaflet
      leaflet: {
        waypoints: llWaypoints,
        forecastPath: llPath,
        path: llPath,
        positionMarker: llPositionMarker,
        locationPopup: llLocationPopup,
        warningMarkers: llWarningMarkers,
        imagePlayer: llImagePlayer,
        vectorPlayer: llVectorPlayer,
        coastlines: llCoastlines
      },
      // forecast graph methods
      echarts: {},
      // webgl map interface
      webgl: {
        map: webgl_map
      }
    },
    lookup: {
      timeStepLookup: timeStepLookup
    }
  }

window.SeaMon = SeaMon;

export default SeaMon;
