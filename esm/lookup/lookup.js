import { kdTree, BinaryHeap } from 'kd-tree-javascript';

function distance2D(a,b) {
    return Math.pow(a.x - b.x, 2) +  Math.pow(a.y - b.y, 2);  
}

function distance1D(a,b) {
    return Math.pow(a.x - b.x, 2);
}

function create2DLookup(xs, ys) {
    let points = [];
    for(let i=0; i<xs.length; i++) {
        points[i] = {x: xs[i], y: ys[i], idx: i};
    }
    let tree = new kdTree(points, distance2D, ['x', 'y']);
    return tree;
}

function create1DLookup(xs) {
    let points = [];
    for(let i=0; i<xs.length; i++) {
        points[i] = {x: xs[i], idx: i};
    }
    let tree = new kdTree(points, distance1D, ['x']);
    return tree;
}

function timeStepLookup(ts) {
    return create1DLookup(ts);
}

export { create1DLookup, create2DLookup, timeStepLookup };