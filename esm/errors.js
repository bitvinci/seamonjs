// define this api error type
function api_error(json) {
    let err = new Error(json.error.message);
    err.code = json.error.code;
    err.apiError = true;
    return err;
  }

export default api_error;