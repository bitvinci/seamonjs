"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatValue = formatValue;

var _sprintfJs = require("sprintf-js");

var _styles = require("./styles");

function round(val, prec) {
  return Math.round(val / prec) * prec;
}

var formatters = {
  "default": function _default(val) {
    return val.toString();
  },
  wind: function wind(val) {
    return (0, _sprintfJs.sprintf)("%.1f", round(val, 0.1));
  },
  gust: function gust(val) {
    return (0, _sprintfJs.sprintf)("%.1f", round(val, 0.1));
  },
  cloud: function cloud(val) {
    return (0, _sprintfJs.sprintf)("%3.0f", round(val, 1));
  },
  rain: function rain(val) {
    return (0, _sprintfJs.sprintf)("%.0f", round(val, 1));
  },
  snow: function snow(val) {
    return (0, _sprintfJs.sprintf)("%.1f", round(val, 0.1));
  },
  temp: function temp(val) {
    return (0, _sprintfJs.sprintf)("%.1f", round(val, 0.1));
  },
  precr: function precr(val) {
    return (0, _sprintfJs.sprintf)("%.1f", round(val, 0.1));
  },
  rainr: function rainr(val) {
    return (0, _sprintfJs.sprintf)("%.1f", round(val, 0.1));
  },
  snowr: function snowr(val) {
    return (0, _sprintfJs.sprintf)("%.1f", round(val, 0.1));
  },
  'rainr-10km-max': function rainr10kmMax(val) {
    return (0, _sprintfJs.sprintf)("%.1f", round(val, 0.1));
  },
  'snowr-10km-max': function snowr10kmMax(val) {
    return (0, _sprintfJs.sprintf)("%.1f", round(val, 0.1));
  },
  vis: function vis(val) {
    if (val > 999) {
      return ">999";
    }

    return (0, _sprintfJs.sprintf)("%.0f", round(val, 1));
  },
  road: function road(val) {
    return (0, _styles.generateStyle)(val, 'road').code;
  }
};

function formatValue(value, parType) {
  if (parType in formatters) {
    return formatters[parType](value);
  }

  return formatters["default"](value);
}