"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _errors = _interopRequireDefault(require("../errors"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/* location forecast query */
function location(pos) {
  var start = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var stop = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var params = arguments.length > 3 ? arguments[3] : undefined;
  var opts = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  // force default opts
  if (!opts.server) opts.server = 'https://api.seamon.io';
  if (!opts.token) opts.token = '';
  if (!opts.save) opts.save = false;
  if (!opts.allowMissingPar) opts.allowMissingPar = false;
  if (!opts.limit) opts.limit = null; // compose URL query - -

  var url = opts.server + '/location?'; // add lat lon pos arg

  if (Array.isArray(pos)) {
    url += "pos=(".concat(pos[0], ";").concat(pos[1], ")");
  } else {
    url += "pos=".concat(pos);
  } // start/stop


  if (start) url += "&start=".concat(start);else url += "&start=now";
  if (stop) url += "&stop=".concat(stop); // add forecast parameter selection

  url += "&params=";
  params.forEach(function (x) {
    url += "".concat(x, ",");
  }); // limit time steps,

  if (opts.limit) url += "&limit=".concat(opts.limit); // add token

  url += "&token=" + opts.token; // add missing pars allowed, if so,

  if (opts.allowMissingPar) url += "&allow_missing_par";
  console.log("query: " + url); // query seamon

  return fetch(url)["catch"](function (e) {
    console.error("fetch had error in ".concat(fcmethod), e);
    return Promise.reject(new Error("failed to reach seamon-api service - check internet connection"));
  }).then(function (resp) {
    // check here for unexpected HTTP errors . e.g. 502
    if ([502, 504].includes(resp.status)) {
      return Promise.reject(new Error("".concat(resp.status, " gateway error - service may be down - admin will be notified")));
    } // all ok so pass on...


    return resp;
  }).then(function (resp) {
    return resp.json();
  }) // remember... .then() is sugar for await...
  .then(function (json) {
    if ('error' in json) {
      // so this is an API error, so
      // pass rejected promise with API error
      return Promise.reject((0, _errors["default"])(json));
    }

    return json;
  });
}

var _default = location;
exports["default"] = _default;