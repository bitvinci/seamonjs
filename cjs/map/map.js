"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _errors = _interopRequireDefault(require("../errors"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/* basic forecast query */
function map(gridll) {
  var start = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var stop = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var params = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  var opts = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  // force default opts
  if (!opts.server) opts.server = 'https://api.seamon.io';
  if (!opts.token) opts.token = '';
  if (!opts.save) opts.save = false;
  if (!opts.allowMissingPar) opts.allowMissingPar = false;
  if (!opts.proj4) opts.proj4 = null;
  if (!opts.view) opts.view = 'json';
  if (!opts.scales) opts.scales = null;
  if (!opts.latlons) opts.latlons = null;
  if (!opts.scaleInterpolation) opts.scaleInterpolation = 'linear';
  if (!opts.antialias) opts.antialias = 0; // compose URL query

  var url = opts.server + '/map?gridll=';
  var gdef = '(';
  gridll.forEach(function (x) {
    gdef += x + ",";
  });
  gdef = gdef.slice(0, -1);
  gdef += ')';
  url += gdef; // add params...

  if (params) {
    var pdef = "&params=";
    params.forEach(function (x) {
      pdef += x + ",";
    });
    pdef = pdef.slice(0, -1);
    url += pdef;
  } // add proj4


  if (opts.proj4) {
    url += "&proj4=".concat(opts.proj4);
  } // start/stop


  if (start) {
    url += "&start=".concat(start);
  }

  if (stop) {
    url += "&stop=".concat(stop);
  } // ask for latlong grid values...


  if (opts.latlons) {
    url += "&latlons";
  } // add any color scales rendering options...


  if (opts.scales) {
    url += "&scales=".concat(opts.scales);
  } // set scale interpolation option,


  if (opts.scaleInterpolation) {
    url += "&scale_interpolation=".concat(opts.scaleInterpolation);
  } // set scale interpolation option,


  if ('antialias' in opts) {
    url += "&antialias=".concat(opts.antialias);
  } // add the image view...


  url += "&view=".concat(opts.view); // add token

  if (opts.token) {
    url += "&token=" + opts.token;
  } // add missing pars allowed, if so,
  // if(opts.allowMissingPar) url += "&allow_missing_par"


  console.log("query: " + url); // query seamon

  return fetch(url)["catch"](function (e) {
    console.error("fetch had error in ".concat(fcmethod), e);
    return Promise.reject(new Error("failed to reach seamon-api service - check internet connection"));
  }).then(function (resp) {
    // check here for unexpected HTTP errors . e.g. 502
    if ([502, 504].includes(resp.status)) {
      return Promise.reject(new Error("".concat(resp.status, " gateway error - service may be down - admin will be notified")));
    } // all ok so pass on...


    return resp;
  }).then(function (resp) {
    return resp.json();
  }) // remember... .then() is sugar for await...
  .then(function (json) {
    if ('error' in json) {
      // so this is an API error, so
      // pass rejected promise with API error
      return Promise.reject((0, _errors["default"])(json));
    }

    return json;
  });
}

var _default = map;
exports["default"] = _default;