"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _road = _interopRequireDefault(require("./route/road"));

var _gca = _interopRequireDefault(require("./route/gca"));

var _location = _interopRequireDefault(require("./location/location"));

var _map = _interopRequireDefault(require("./map/map"));

var _path = _interopRequireDefault(require("./path/path"));

var _waypoints = _interopRequireDefault(require("./graphics/leaflet/waypoints"));

var _path2 = _interopRequireDefault(require("./graphics/leaflet/path"));

var _imagePlayer = _interopRequireDefault(require("./graphics/leaflet/imagePlayer"));

var _vectorPlayer = _interopRequireDefault(require("./graphics/leaflet/vectorPlayer"));

var _positionMarker = _interopRequireDefault(require("./graphics/leaflet/positionMarker"));

var _warningMarkers = _interopRequireDefault(require("./graphics/leaflet/warningMarkers"));

var _locationPopup = _interopRequireDefault(require("./graphics/leaflet/locationPopup"));

var _coastlines = _interopRequireDefault(require("./graphics/leaflet/coastlines"));

var _styles = require("./styles/styles");

var _svg = require("./styles/svg");

var _format = require("./styles/format");

var _lookup = require("./lookup/lookup");

var _map2 = _interopRequireDefault(require("./graphics/webgl/map.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// define namespaces
var SeaMon = {
  route: {
    road: _road["default"],
    gca: _gca["default"]
  },
  location: _location["default"],
  map: _map["default"],
  path: _path["default"],
  // fc model query data
  models: {},
  // seamon-api version
  version: null,
  // forecast plot implementations for user convenience
  // e.g. generating forecast graphs and webgis paths
  styles: {
    generateStyle: _styles.generateStyle,
    generateCritical: _styles.generateCritical,
    criticalStyle: _styles.criticalStyle,
    getWeatherIcon: _svg.getWeatherIcon,
    getIcon: _svg.getWeatherIcon,
    weatherIcons: _svg.weatherIcons,
    icons: _svg.weatherIcons,
    formatValue: _format.formatValue
  },
  graphics: {
    // webgis plot methods for leaflet
    leaflet: {
      waypoints: _waypoints["default"],
      forecastPath: _path2["default"],
      path: _path2["default"],
      positionMarker: _positionMarker["default"],
      locationPopup: _locationPopup["default"],
      warningMarkers: _warningMarkers["default"],
      imagePlayer: _imagePlayer["default"],
      vectorPlayer: _vectorPlayer["default"],
      coastlines: _coastlines["default"]
    },
    // forecast graph methods
    echarts: {},
    // webgl map interface
    webgl: {
      map: _map2["default"]
    }
  },
  lookup: {
    timeStepLookup: _lookup.timeStepLookup
  }
};
window.SeaMon = SeaMon;
var _default = SeaMon;
exports["default"] = _default;