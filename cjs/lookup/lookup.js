"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.create1DLookup = create1DLookup;
exports.create2DLookup = create2DLookup;
exports.timeStepLookup = timeStepLookup;

var _kdTreeJavascript = require("kd-tree-javascript");

function distance2D(a, b) {
  return Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2);
}

function distance1D(a, b) {
  return Math.pow(a.x - b.x, 2);
}

function create2DLookup(xs, ys) {
  var points = [];

  for (var i = 0; i < xs.length; i++) {
    points[i] = {
      x: xs[i],
      y: ys[i],
      idx: i
    };
  }

  var tree = new _kdTreeJavascript.kdTree(points, distance2D, ['x', 'y']);
  return tree;
}

function create1DLookup(xs) {
  var points = [];

  for (var i = 0; i < xs.length; i++) {
    points[i] = {
      x: xs[i],
      idx: i
    };
  }

  var tree = new _kdTreeJavascript.kdTree(points, distance1D, ['x']);
  return tree;
}

function timeStepLookup(ts) {
  return create1DLookup(ts);
}