"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _three = require("three");

var _OrbitControls = require("three/examples/jsm/controls/OrbitControls");

var _getCoastlines = _interopRequireDefault(require("./getCoastlines"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _construct(Parent, args, Class) { if (_isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var loader = new _three.TextureLoader();

var Map = /*#__PURE__*/function () {
  _createClass(Map, [{
    key: "width",
    value: function width() {
      return this.element.offsetWidth;
    }
  }, {
    key: "height",
    value: function height() {
      return this.element.offsetHeight;
    }
  }]);

  function Map() {
    var _this = this;

    _classCallCheck(this, Map);

    this.element = document.getElementById("webgl-div");
    var width = this.element.offsetWidth;
    var height = this.element.offsetHeight;
    this.scene = new _three.Scene(); // set up an orthographic camera

    var aspect = width / height;
    this.camera = new _three.OrthographicCamera(-aspect * 1, aspect * 1, 1, -1, 0.01, 100);
    this.renderer = new _three.WebGLRenderer({
      antialias: true
    });
    this.renderer.setSize(width, height);
    this.element.appendChild(this.renderer.domElement); // camera controls...

    this.controls = new _OrbitControls.OrbitControls(this.camera, this.renderer.domElement);
    this.controls.enablePan = false;
    this.rotateSpeed = 0.3;
    this.controls.rotateSpeed = this.rotateSpeed;
    this.camera.position.z = 10; // lookat objects are for objects that should orient
    // towards the camera - e.g. text.

    this.lookAtObjects = [];

    this.render = function () {
      _this.renderer.render(_this.scene, _this.camera);

      _this.lookAtObjects.forEach(function (ob) {
        ob.lookAt(_this.camera.position.x, _this.camera.position.y, _this.camera.position.z);
      });

      var z = _this.camera.zoom;
      _this.controls.rotateSpeed = _this.rotateSpeed / z;
    }; /////////////////////


    add_sphere(this.scene, 0.995); //add_image(this.scene, 0.998);

    add_graticules(this.scene); /////////////////////
    // Only render scene on camera viw change...
    // because scene is static
    // for animations need activate animation frame code...

    this.controls.addEventListener('change', this.render);
    this.render(); // initial render...
    // on window resize, must reset and redraw...

    this.resize = function () {
      console.log("resizing webgl map");

      var width = _this.width();

      var height = _this.height();

      var aspect = width / height;

      _this.renderer.setSize(width, height); // enforce the orthographic camera aspect ratio


      _this.camera.left = _this.camera.bottom * aspect;
      _this.camera.right = _this.camera.top * aspect;

      _this.camera.updateProjectionMatrix();

      _this.render();
    };

    window.addEventListener('resize', this.resize);
  }
  /* fetches and adds coastline data */


  _createClass(Map, [{
    key: "coastlines",
    value: function coastlines() {
      var _this2 = this;

      (0, _getCoastlines["default"])().then(function (tj) {
        console.log("hello");
        add_topojson_lines(_this2.scene, tj);

        _this2.render();
      });
    }
  }, {
    key: "setCameraPosition",
    value: function setCameraPosition(lat, lon) {
      var u = lon * Math.PI / 180.0;
      var v = lat * Math.PI / 180.0; //let pi2 = Math.PI/2.0;

      var r = 10.0;
      var x = Math.cos(v) * Math.sin(u);
      var y = Math.sin(v);
      var z = Math.cos(v) * Math.cos(u);
      console.log(this.camera.position);
      this.camera.position.set(r * x, r * y, r * z);
      console.log(this.camera.position);
      this.controls.update();
      console.log(this.camera.position);
    }
  }, {
    key: "imageTile",
    value: function imageTile(img, bbox) {
      var _this3 = this;

      var res = add_image(this.scene, 0.999, img, bbox);
      var tile = res.object;
      var avLat = res.avLat;
      var avLon = res.avLon;
      this.setCameraPosition(avLat, avLon); // fit fov/zoom height to this tile...

      var dLat = res.dLat;
      var gH = Math.sin(dLat * Math.PI / 180.0);
      var zoom = 2.0 / gH;
      this.camera.zoom = zoom;
      this.camera.updateProjectionMatrix();
      this.render(); // quick fix (texture loader async problem)...

      setTimeout(function () {
        _this3.render();
      }, 50);
    }
  }]);

  return Map;
}();

function map() {
  return new Map();
}

var _default = map; // lat long to xyz (length scale 1 == earth radius)

exports["default"] = _default;

function ll2xyz(lonlat) {
  var lonr = lonlat[0] * Math.PI / 180.0;
  var latr = lonlat[1] * Math.PI / 180.0;
  return [Math.cos(latr) * Math.sin(lonr), Math.sin(latr), Math.cos(latr) * Math.cos(lonr)];
} // lat long to xyz (height as fraction of earth radius)


function ll2xyz_height(lonlat, height) {
  var lonr = lonlat[0] * Math.PI / 180.0;
  var latr = lonlat[1] * Math.PI / 180.0;
  return [height * Math.cos(latr) * Math.sin(lonr), height * Math.sin(latr), height * Math.cos(latr) * Math.cos(lonr)];
} // add polyline to map


function add_polyline(scene, coords, material) {
  var closed = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  var points = [];

  for (var i = 0; i < coords.length; i++) {
    var xyz = ll2xyz(coords[i]); // note the topojson coords are (lon,lat) order

    points.push(_construct(_three.Vector3, _toConsumableArray(xyz)));
  } // close the line loop if closed==true by appending the first vertice


  if (closed) points.push(points[0]);
  var geometry = new _three.BufferGeometry().setFromPoints(points);
  var poly_line = new _three.Line(geometry, material);
  scene.add(poly_line);
} // Add great circle path between points on sphere, controlled by definition in km
// This method should be used when plotting lines over long distances on the sphere.  


function add_great_circle_polyline(coords, material) {
  var closed = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  var definition = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 500.0;
} // add base sphere


function add_sphere(scene, rad) {
  var geometry = new _three.SphereGeometry(rad, 64, 64);
  var material = new _three.MeshBasicMaterial({
    color: 0x002222
  });
  var sphere = new _three.Mesh(geometry, material);
  scene.add(sphere);
}

function add_image(scene, rad, img, bbox) {
  // create mesh tile for image...
  var lon0 = bbox[0];
  var dlon = bbox[2] - bbox[0];
  var lat0 = bbox[1];
  var dlat = bbox[1] - bbox[3];
  var u = (lon0 + 90.0) / 180.0 * Math.PI;
  var v = (90.0 - lat0) / 180.0 * Math.PI;
  var du = dlon / 180.0 * Math.PI;
  var dv = dlat / 180.0 * Math.PI;
  var geometry = new _three.SphereGeometry(rad, 64, 64, u, du, v, dv);
  var material = new _three.MeshBasicMaterial({
    /*color: 0xff0000*/
    map: loader.load(img)
  });
  var tile = new _three.Mesh(geometry, material);
  scene.add(tile);
  return {
    object: tile,
    avLat: lat0 - dlat / 2.0,
    avLon: lon0 + dlon / 2.0,
    dLat: dlat,
    dLon: dlon
  };
} // add graticules


function add_parallel(scene, deg, material) {
  var n = 60;
  var coords = [];

  for (var i = 0; i < n; i++) {
    var lat = deg;
    var lon = i * 360.0 / n;
    coords[i] = [lon, lat];
  }

  add_polyline(scene, coords, material, true);
}

function add_meridian(scene, deg, material) {
  var min_lat = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : -80.0;
  var max_lat = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 80.0;
  var n = 60;
  var coords = [];

  for (var i = 0; i <= n; i++) {
    var lon = deg;
    var lat = min_lat + i * 2.0 * max_lat / n;
    coords[i] = [lon, lat];
  }

  add_polyline(scene, coords, material, true);
}

function add_graticules(scene) {
  var material = new _three.LineBasicMaterial({
    color: 0x505050,
    linewidth: 0.5
  });

  for (var lat = -80; lat <= 80; lat += 10) {
    add_parallel(scene, lat, material);
  }

  for (var lon = 0; lon < 360; lon += 10) {
    if (lon % 90 == 0) add_meridian(scene, lon, material, -90, 90);else add_meridian(scene, lon, material, -80, 80);
  }
}

function add_topojson_lines(scene, tj) {
  var material = new _three.LineBasicMaterial({
    color: 0x00ffff,
    linewidth: 0.73
  });
  tj.features.forEach(function (f) {
    var coords = f.geometry.coordinates;
    add_polyline(scene, coords, material, false);
  });
} // Load coastlines from geojson

/*function add_coastlines() {
queue()
    .defer(d3.json, "/static/world-110m.json")
    .await( function (e, world) {
                console.log("loading coastlines");
                var material = new THREE.LineBasicMaterial( {color: 0x00ffff, linewidth:0.73} );
                var land_data = topojson.object(world, world.objects.land);
                const coords = land_data.coordinates;

                for (var i=0; i<coords.length; i++)
                    for (var k=0; k<coords[i].length; k++)
                    add_polyline(land_data.coordinates[i][k], material);

                // update land/coastline data
                //svg.select("path.land")
                //    .datum(topojson.object(world, world.objects.land))
                //   .attr("d", path);

                // update countries outlines
                //svg.select("g.countries")
                //    .selectAll("path")
                //    .data(topojson.object(world, world.objects.countries).geometries)
                //    .attr("d", path);
            });
}

// draw waypoint locations and waypoint paths
// waypoint locations
waypts = [
% for _, routeData in data['waypts'].iteritems():
% for pt in routeData:
    % if pt['label'] != "":
    { label:"${pt['label']}", lat:${pt['lon']}, lon:${pt['lat']} },
    % endif
% endfor
% endfor
];

wayptsPaths = [
% for _, routeData in data['waypts'].iteritems():
[
% for pt in routeData:
    [${pt['lon']}, ${pt['lat']}],
% endfor
],
% endfor
];

function add_waypoint_location(lat, lon, label) {
// location point
var geometry = new THREE.SphereGeometry( 0.01, 8, 8 );
var material = new THREE.MeshBasicMaterial( {color: 0xffffff} );
var marker = new THREE.Mesh( geometry, material );
xyz = ll2xyz_height([lon,lat],1.0);
marker.position.set(...xyz);
scene.add( marker );

// location label
var loader = new THREE.FontLoader();
loader.load( '/static/helvetiker_regular.typeface.json', function(font) {
    var geometry = new THREE.TextGeometry( label, {
    font: font,
    size: 0.05,
    height: 0.0,
    //curveSegments: 12,
    //bevelEnabled: true,
    //bevelThickness: 10,
    //bevelSize: 8,
    //bevelSegments: 5
    });
    var txt = new THREE.Mesh( geometry, material );
    xyz = ll2xyz_height([lon,lat],0.05);
    txt.position.set(...xyz);
    marker.add(txt);
    lookAtObjects.push(txt);
});

return marker;
}

function add_ship() {
if ( waypts.length>0 ) ship = add_waypoint_location(waypts[0].lon, waypts[0].lat, "ship");
else ship = add_waypoint_location(0.0, 0.0, "ship");
hideShip();
}

function hideShip() {
ship.visible = false;
} 

function updateShipPos(lon, lat) {
ship.visible = true;
var xyz = ll2xyz([lon, lat]);
ship.position.set(...xyz);
}

// set up scene objects
add_sphere(0.995);
add_graticules();
add_coastlines();

// waypoint locations
for (var i=0; i<waypts.length; i++) add_waypoint_location(waypts[i].lon, waypts[i].lat, waypts[i].label);

// waypoint paths
var material = new THREE.LineBasicMaterial( {color: 0xFF0000} );
for (var i=0; i<wayptsPaths.length; i++) {
const coords = wayptsPaths[i];
var gc_coords = great_circle_path(coords);
add_polyline(gc_coords, material);
}

// ship marker
add_ship();




//========== haversine math ==============
r_earth = 6371.0; // in km

function radians(angle) {
return angle*Math.PI/180.0;
}

function degrees(angle) {
return angle*180.0/Math.PI;
}

// return angular distance between points on sphere in radians
function haversine_angular_distance(lonlat1, lonlat2) {
var latRad1 = radians(lonlat1[1]);
var latRad2 = radians(lonlat2[1]);
var DlatRad = latRad2 - latRad1;
var DlonRad = radians(lonlat2[0] - lonlat1[0]);
var a = Math.pow(Math.sin(DlatRad/2),2) + Math.cos(latRad1)*Math.cos(latRad2) * Math.pow(Math.sin(DlonRad/2),2);
var one_minus_a = 1-a;
if (one_minus_a < 0.0) one_minus_a = 0.0; // avoiding rounding error
return 2*Math.atan2(Math.sqrt(a), Math.sqrt(one_minus_a));
}

// returns great circle distance in km between points 
function haversine_distance(lonlat1, lonlat2) {
return r_earth*haversine_angular_distance(lonlat1, lonlat2);
}

// Intermetiate point a fraction f along great circle path between points
function intermediate_point(lonlat1, lonlat2, f) {
var latRad1 = radians(lonlat1[1]);
var latRad2 = radians(lonlat2[1]);
var lonRad1 = radians(lonlat1[0]);
var lonRad2 = radians(lonlat2[0]);
var DlatRad = latRad2 - latRad1;
var DlonRad = lonRad2 - lonRad1;
var angDist = haversine_angular_distance(lonlat1, lonlat2);
if (angDist == 0.0) angDist = 1.0e-7; // make smaller than a meter on the sphere, but never zero

var a = Math.sin((1-f)*angDist) / Math.sin(angDist);
var b = Math.sin(f*angDist) / Math.sin(angDist);

var x = a*Math.cos(latRad1)*Math.cos(lonRad1) + b*Math.cos(latRad2)*Math.cos(lonRad2);
var y = a*Math.cos(latRad1)*Math.sin(lonRad1) + b*Math.cos(latRad2)*Math.sin(lonRad2);
var z = a*Math.sin(latRad1) + b*Math.sin(latRad2);

var latRadPt = Math.atan2(z, Math.sqrt(x*x+y*y));
var lonRadPt = Math.atan2(y, x);
return [degrees(lonRadPt), degrees(latRadPt)];
}

function great_circle_path(coords, definition=500.0) {
// loop over coordinate pairs
var gc_coords = [];

for (var i=0; i<coords.length-1; i++) {
    var a = coords[i];
    var b = coords[i+1];
    let dist = haversine_distance(a,b);

    // add starting point
    gc_coords.push(a);

    // add intermediate points if needed (up to but not including destination point)
    if (dist > definition) {
        let steps = parseInt(dist/definition);
        for (k=1; k<steps; k++) {
        let f = k * definition/dist;
        gc_coords.push( intermediate_point(a, b, f) );
        } 
    }
}
//finally add the final destination point
gc_coords.push(b);
return gc_coords;
}*/
//========================================