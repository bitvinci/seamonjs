"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _leaflet = _interopRequireDefault(require("leaflet"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var VectorPlayer = /*#__PURE__*/function () {
  function VectorPlayer(data, model, ampPar, dirPar) {
    var map = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : null;
    var opts = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};

    _classCallCheck(this, VectorPlayer);

    this._class_name = "VectorPlayer"; // opts defaults

    if (!opts.weight) opts.weight = 2.0;
    if (!opts.color) opts.color = "black";
    if (!opts.size) opts.size = 1.0;
    if (!opts.length) opts.length = 1.0;
    if (!opts.minAmp) opts.minAmp = null;
    if (!opts.maxAmp) opts.maxAmp = null;
    if (!opts.featherOpacity) opts.featherOpacity = null;
    if (!opts.opacity) opts.opacity = 1.0;
    if (!opts.pane) opts.pane = null; // TODO: add forecast response type check
    // because this method only applies to map response data
    // create vector features...

    this.vectors = createVectors(data, model, ampPar, dirPar, map, opts); // collect time stamps

    var times = data.forecasts[model].times;
    var timesJS = [];
    times.forEach(function (t) {
      timesJS.push(new Date(t * 1000.0));
    }); // store JS time stamps

    this.times = times;
    this.timesJS = timesJS;
    this.idx = 0; // first vector image displayed
    // if map provided add ol to map

    this.maps = [];

    if (map) {
      this.vectors[this.idx].addTo(map); // keep track of attached maps...

      this.maps.push(map);
    } // create time step lookup,


    var timeLookup = SeaMon.lookup.timeStepLookup(times);
    this._timeLookup = timeLookup;
  }

  _createClass(VectorPlayer, [{
    key: "length",
    value: function length() {
      return this.times.length;
    }
  }, {
    key: "getBounds",
    value: function getBounds() {
      return this.vectors[0].getBounds();
    }
  }, {
    key: "setIndex",
    value: function setIndex(idx) {
      var _this = this;

      if (idx > this.length()) {
        console.error(this._class_name + ":cant show, index ".concat(idx, " exceeds length of forecast"));
        return;
      }

      var prevIdx = this.idx;
      this.maps.forEach(function (m) {
        // flip vectors...
        console.log("TODO, flip vectors");
        m.removeLayer(_this.vectors[prevIdx]);

        _this.vectors[idx].addTo(m);
      });
      this.idx = idx;
    }
  }, {
    key: "lookup",
    value: function lookup(t) {
      return this._timeLookup.nearest({
        x: t
      }, 1)[0][0].idx;
    }
  }, {
    key: "setTime",
    value: function setTime(t) {
      var idx = this.lookup(t);
      this.setIndex(idx);
    }
  }, {
    key: "time",
    value: function time() {
      return this.times[this.idx];
    }
  }, {
    key: "timeJS",
    value: function timeJS() {
      return this.timesJS[this.idx];
    }
  }, {
    key: "next",
    value: function next() {
      this.idx = (this.idx + 1) % this.length();
      this.setIndex(this.idx);
    }
  }, {
    key: "addTo",
    value: function addTo(map) {
      this.vectors[this.idx].addTo(map); // keep track of attached maps...

      this.maps.push(map);
    }
  }, {
    key: "remove",
    value: function remove() {
      var _this2 = this;

      var map = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      if (map) {
        map.removeLayer(this.vectors[this.idx]);
        this.maps = this.maps.filter(function (item) {
          return item !== map;
        });
        return;
      } // else remove from all maps in the maps array,


      this.maps.forEach(function (x) {
        x.removeLayer(_this2.vectors[_this2.idx]);
      });
      this.maps = [];
    }
  }]);

  return VectorPlayer;
}();

function vectorPlayer(data, model, ampPar, dirPar, map) {
  var opts = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};
  var player = new VectorPlayer(data, model, ampPar, dirPar, map, opts);
  return player;
}

function createVectors(data, model, ampPar, dirPar, map) {
  var opts = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : {};
  var funcName = "createVectors:"; // opts defaults

  if (!opts.weight) opts.weight = 2.0;
  if (!opts.color) opts.color = "black";
  if (!opts.opacity) opts.opacity = 1.0;
  if (!opts.size) opts.size = 1.0;
  if (!opts.length) opts.length = 1.0;
  if (!opts.minAmp) opts.minAmp = null;
  if (!opts.maxAmp) opts.maxAmp = null;
  if (!opts.featherOpacity) opts.featherOpacity = null; // model data

  var fcDat = data.forecasts[model]; // check if data available

  if (!('lats' in fcDat)) {
    console.error(funcName + "no lat grid available in data");
    return null;
  }

  if (!('lons' in fcDat)) {
    console.error(funcName + "no lon grid available in data");
    return null;
  }

  if (ampPar && !(ampPar in fcDat.members[model])) {
    console.error(funcName + "amplitude paremeter not in data");
    return null;
  }

  if (!(dirPar in fcDat.members[model])) {
    console.error(funcName + "direction paremeter not in data");
    return null;
  } // basic vector shape features...
  // TODO: check geographic meaning of sizes in leaflet...


  var baseSize = 200.0;
  var size = opts.size * baseSize;
  var length = opts.length; // length factor for vectors
  // using vector amplitudes?

  var usingAmps = false;
  var minAmp = opts.minAmp; // skip drawing if < this amplitude

  var maxAmp = opts.maxAmp; // skip drawing if > this amplitude

  var featherOpacity = opts.featherOpacity;
  var opacity = opts.opacity;
  var ampMax = 0.0;

  if (ampPar) {
    usingAmps = true; // get the max from first time step...
    // helps normalize the vectors w.r.t. size option.
    // TODO: this could be improved...

    ampMax = Math.max.apply(Math, _toConsumableArray(fcDat.members[model][ampPar][0]));
    console.log("max amp is", ampMax);
  }

  var lats = data.forecasts[model].lats;
  var lons = data.forecasts[model].lons;
  var Nsteps = data.forecasts[model].times.length; // create arrow line features for leaflet

  var fGroups = [];

  for (var i = 0; i < Nsteps; i++) {
    // always use direction param
    var dir = fcDat.members[model][dirPar][i]; // use amplitude of vector if provided

    var amp = null;
    if (usingAmps) amp = fcDat.members[model][ampPar][i];

    var fg = _leaflet["default"].featureGroup(); // loop through wind data creating lines


    var arrowSize = size;
    var wingSize1 = size / 8.0;
    var wingSize2 = size / 3.0;

    for (var _i = 0; _i < dir.length; _i++) {
      // if null 'masked' map value,
      // then skip forward to next vector,
      if (dir[_i] == null) continue; // start point in latlon

      var ll0 = {
        lat: lats[_i],
        lng: lons[_i]
      }; // always use direction parameter

      var theta = dir[_i] * Math.PI / 180.0; // use amplitude if provided

      var A = null;
      var thisOpacity = opacity;

      if (amp) {
        A = amp[_i] / ampMax; // normalize length of vector

        if (minAmp && amp[_i] < minAmp) continue;
        if (maxAmp && amp[_i] > maxAmp) continue;

        if (featherOpacity) {
          if (amp[_i] > featherOpacity) thisOpacity = opacity;else {
            thisOpacity = amp[_i] / featherOpacity * opacity;
          }
        }
      } //// Draw arrows in target projection
      //// Ensures they are sized by the map (not latlon)
      // project arrow origin point to map grid


      var p0 = map.project(ll0, 10); // stem point of arrow,
      // affected by amplitude if provided

      var thisArrowLen = arrowSize * length;

      if (A) {
        thisArrowLen *= A;
      }

      var p1 = {
        x: p0.x,
        y: p0.y + thisArrowLen
      }; // wing of arrow

      var w1 = {
        x: p0.x + wingSize1,
        y: p0.y + wingSize2
      };
      var w2 = {
        x: p0.x - wingSize1,
        y: p0.y + wingSize2
      }; // rotate

      p1 = rotate(p0, p1, theta);
      w1 = rotate(p0, w1, theta);
      w2 = rotate(p0, w2, theta); // project back to second point in latlon

      var ll1 = map.unproject(p1, 10);
      var wll1 = map.unproject(w1, 10);
      var wll2 = map.unproject(w2, 10); // create lines

      var stem = new _leaflet["default"].Polyline([ll0, ll1], {
        color: opts.color,
        weight: opts.weight,
        opacity: thisOpacity
      });
      var head = new _leaflet["default"].Polyline([wll1, ll0, wll2], {
        color: opts.color,
        weight: opts.weight,
        opacity: thisOpacity
      });

      if (opts.pane) {
        stem.options.pane = opts.pane;
        head.options.pane = opts.pane;
      }

      stem.addTo(fg);
      head.addTo(fg);
    }

    fGroups.push(fg);
  }

  return fGroups;
} // roatate point p1 w.r.t. p0


function rotate(p0, p1, angle) {
  var S = Math.sin(angle);
  var C = Math.cos(angle);
  var dx = p1.x - p0.x;
  var dy = p1.y - p0.y;
  var dxr = dx * C + dy * S;
  var dyr = dx * S - dy * C;
  return {
    x: p0.x + dxr,
    y: p0.y + dyr
  };
}

var _default = vectorPlayer;
exports["default"] = _default;