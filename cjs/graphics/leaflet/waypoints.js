"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _leaflet = _interopRequireDefault(require("leaflet"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function waypoints(data, map) {
  var svgIcon = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var markersGroup = new _leaflet["default"].featureGroup(); // set svg icon

  var icon = "<svg xmlns=\"http://www.w3.org/2000/svg\" height=\"24\" viewBox=\"0 0 24 24\" width=\"24\">\n                <path d=\"M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z\"/><path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n                </svg>";
  if (svgIcon) icon = svgIcon;

  var PosIcon = _leaflet["default"].icon({
    iconUrl: 'data:image/svg+xml;base64,' + btoa(icon),
    iconSize: [34, 34],
    // size of the icon
    iconAnchor: [17, 30],
    popupAnchor: [0, -20]
  }); // collect markers


  var waypointMarkers = [];

  for (var route in data.waypts) {
    data.waypts[route].forEach(function (wp) {
      // markers have labels...
      if (wp.label != "") {
        var n = waypointMarkers.length;
        waypointMarkers[n] = {
          'lat': wp.lat,
          'lon': wp.lon,
          'label': wp.label
        }; // add to marker group

        var pop = _leaflet["default"].popup({
          closeButton: false,
          className: 'loclabel'
        }).setContent(wp.label);

        _leaflet["default"].marker([wp.lat, wp.lon], {
          icon: PosIcon
        }).bindPopup(pop).addTo(markersGroup);
      }
    });
  }

  if (map) markersGroup.addTo(map);
  return markersGroup;
}

var _default = waypoints;
exports["default"] = _default;