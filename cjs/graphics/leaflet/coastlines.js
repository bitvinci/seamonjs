"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _leaflet = _interopRequireDefault(require("leaflet"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// quick and dirty coaslines plotting
// for leaflet based on geojson blob
// TODO: implement a more flexible coastlines service within seamon.
function coastlines(map, bbox, opts) {
  // get the coaslines geojson
  var url = 'https://api.seamon.io/mapdata/ne_10m_coastline.geojson';
  return fetch(url)["catch"](function (e) {
    console.error("fetch had error in ".concat(fcmethod), e);
    return Promise.reject(new Error("failed to reach server - check internet connection"));
  }).then(function (resp) {
    // check here for unexpected HTTP errors . e.g. 502
    if ([502, 504].includes(resp.status)) {
      return Promise.reject(new Error("".concat(resp.status, " gateway error - service may be down - admin will be notified")));
    } // all ok so pass on...


    return resp;
  }).then(function (resp) {
    return resp.json();
  }) // converts to json
  .then(function (json) {
    console.log("got geojson data", json); // clip the coastline data by bbox

    var crop = cropCoastlines(json, bbox); // add to map

    _leaflet["default"].geoJSON(crop, opts).addTo(map);
  });
}

function cropCoastlines(d, bbox) {
  var minLon = bbox[0];
  var minLat = bbox[1];
  var maxLon = bbox[2];
  var maxLat = bbox[3];
  var c = {};
  c.bbox = bbox;
  c.type = d.type;
  c.features = [];

  var _loop = function _loop(i) {
    var x = d.features[i];
    var coords = [[]]; // array of coord arrays

    var iSeg = 0; // loop through coords checking if in bbox
    // and break up features that are split by cropping

    x.geometry.coordinates.forEach(function (ll) {
      var lon = ll[0];
      var lat = ll[1];

      if (minLon < lon && lon < maxLon && minLat < lat && lat < maxLat) {
        coords[iSeg].push(ll);
      } else {
        if (coords[iSeg].length > 0) {
          iSeg += 1;
          coords[iSeg] = [];
        }
      }
    });
    coords.forEach(function (co) {
      if (co.length > 1) {
        var f = {
          type: x.type,
          properties: x.properties
        };
        f.bbox = x.bbox;
        f.geometry = {
          type: x.geometry.type,
          coordinates: []
        };
        f.geometry.coordinates = co;
        c.features.push(f);
      }
    });
  };

  for (var i = 0; i < d.features.length; i++) {
    _loop(i);
  }

  return c;
}

var _default = coastlines;
exports["default"] = _default;