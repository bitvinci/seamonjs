"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _leaflet = _interopRequireDefault(require("leaflet"));

var _lookup = _interopRequireDefault(require("../../lookup/lookup"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var ImagePlayer = /*#__PURE__*/function () {
  function ImagePlayer(data, model, par) {
    var map = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
    var opts = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};

    _classCallCheck(this, ImagePlayer);

    this._class_name = "ImagePlayer"; // opts defaults

    if (!opts.opacity) opts.opacity = 1.0;
    if (!opts.pane) opts.pane = null; // TODO: add forecast response type check
    // because this method only applies to JsonImage response data
    // collect base64 png images into array

    var imgs64 = [];
    data.forecasts[model].members[model][par].forEach(function (x) {
      x = 'data:image/png;base64,' + x;
      imgs64.push(x);
    });
    console.log(this._class_name + ":got", imgs64.length, "images");
    this.images = imgs64; // collect time stamps

    var times = data.forecasts[model].times;
    var timesJS = [];
    times.forEach(function (t) {
      timesJS.push(new Date(t * 1000.0));
    }); // store JS time stamps

    this.times = times;
    this.timesJS = timesJS;
    this.idx = 0; // first image displayed
    // create overlay with first image

    var im64 = imgs64[this.idx];
    var boundsll = data.forecasts[model].gridll;
    var imageBounds = [[boundsll[1], boundsll[0]], [boundsll[3], boundsll[2]]];
    var olOpts = {};
    if (opts.pane) olOpts.pane = opts.pane;

    var ol = _leaflet["default"].imageOverlay(im64, imageBounds, olOpts); // store the overlay


    this.imageOverlay = ol; // if map provided add ol to map

    this.maps = [];

    if (map) {
      ol.addTo(map); // keep track of attached maps...

      this.maps.push(map);
    } // create time step lookup,


    var timeLookup = SeaMon.lookup.timeStepLookup(times);
    this._timeLookup = timeLookup;
  }

  _createClass(ImagePlayer, [{
    key: "length",
    value: function length() {
      return this.times.length;
    }
  }, {
    key: "getBounds",
    value: function getBounds() {
      return this.imageOverlay.getBounds();
    }
  }, {
    key: "setIndex",
    value: function setIndex(idx) {
      if (idx > this.length()) {
        console.error(this._class_name + ":cant show image, index ".concat(idx, " exceeds length of forecast"));
        return;
      }

      this.imageOverlay.setUrl(this.images[idx]);
      this.idx = idx;
    }
  }, {
    key: "lookup",
    value: function lookup(t) {
      return this._timeLookup.nearest({
        x: t
      }, 1)[0][0].idx;
    }
  }, {
    key: "setTime",
    value: function setTime(t) {
      var idx = this.lookup(t);
      this.setIndex(idx);
    }
  }, {
    key: "time",
    value: function time() {
      return this.times[this.idx];
    }
  }, {
    key: "timeJS",
    value: function timeJS() {
      return this.timesJS[this.idx];
    }
  }, {
    key: "next",
    value: function next() {
      this.idx = (this.idx + 1) % this.length();
      this.setIndex(this.idx);
    }
  }, {
    key: "addTo",
    value: function addTo(map) {
      ol.addTo(map); // keep track of attached maps...

      this.maps.push(map);
    }
  }, {
    key: "remove",
    value: function remove() {
      var _this = this;

      var map = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      if (map) {
        map.removeLayer(this.imageOverlay);
        this.maps = this.maps.filter(function (item) {
          return item !== map;
        });
        return;
      } // else remove from all maps in the maps array,


      this.maps.forEach(function (x) {
        x.removeLayer(_this.imageOverlay);
      });
      this.maps = [];
    }
  }]);

  return ImagePlayer;
}();
/* takes a jsonimage (base64 encoded) map response from SeaMon-API
   and creates a playable leaflet imageOverlay on the map,
   returning a play controller to step through the images on 
   the map overlay */


function imagePlayer(data, model, par, map) {
  var opts = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : {};
  var funcName = "imagePlayer:"; // play control object

  var player = new ImagePlayer(data, model, par, map, opts);
  return player;
}

var _default = imagePlayer;
exports["default"] = _default;