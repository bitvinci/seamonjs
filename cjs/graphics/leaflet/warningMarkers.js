"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _leaflet = _interopRequireDefault(require("leaflet"));

var _styles = require("../../styles/styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// generate warning markers
function warningMarkers(data, model, map) {
  var opts = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

  var _generateCritical = (0, _styles.generateCritical)(data),
      critical = _generateCritical.critical,
      encs = _generateCritical.encs; // pick out data


  var msgs = encs;
  var lats = data.forecasts[model].lats;
  var lons = data.forecasts[model].lons;
  var style = _styles.stylesState.criticalStyle; // generate markers into feature group

  var fg = _leaflet["default"].featureGroup();

  var prev_i = 0;
  var prev_msg = null; // collect some display settings...

  var eOpts = {};
  if ('pane' in opts) eOpts.pane = opts.pane; // NOTE: Assume msgs come sorted ...

  msgs.forEach(function (x) {
    var i = x.index; // construct warnig msg -- from array

    var msg = "";
    x.msgs.forEach(function (m) {
      m = m.split(',').join('<br/>');
      msg += m + '<br/>';
    }); // if duplicate of previous one within 10 steps abort

    if (i - prev_i < 10) {
      if (msg === prev_msg) return;
    }

    prev_i = i;
    prev_msg = msg; // create marker
    // set svg icon

    var icon = "<svg style=\"fill:".concat(style[x.critical].color, ";\n           filter: drop-shadow(0px 0px 2px black);\"\n           xmlns=\"http://www.w3.org/2000/svg\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\">\n        <path d=\"M0 0h24v24H0z\" fill=\"none\"/>\n        <path d=\"M 12,2 L 23,21 L1,21\" fill=\"black\"/>\n        <path d=\"M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z\"/>\n      </svg>"); //let icon = `<svg style="fill:${style[x.critical].color}" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="white"/><path d="M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z"/></svg>`;

    var PosIcon = _leaflet["default"].icon({
      iconUrl: 'data:image/svg+xml;base64,' + btoa(icon),
      iconSize: [30, 30]
    }); // marker


    var mark = _leaflet["default"].marker(new _leaflet["default"].LatLng(lats[i], lons[i]), _objectSpread({
      icon: PosIcon
    }, eOpts));

    var pop = _leaflet["default"].popup({
      closeButton: false,
      className: 'warnlabel'
    }).setContent("".concat(msg));

    mark.bindPopup(pop, {
      autoClose: false,
      closeOnClick: false
    }).openPopup().addTo(fg);
  });
  if (map) fg.addTo(map);
  return fg;
}

var _default = warningMarkers;
exports["default"] = _default;