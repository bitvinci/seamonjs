"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function positionMarker(map) {
  var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  // set options from opts
  var popup = false;
  if (popup in opts) popup = opts.popup;
  if (!opts.fill) opts.fill = "black";
  var icon = "<svg xmlns=\"http://www.w3.org/2000/svg\"\n                     width=\"24\" height=\"24\" viewBox=\"0 0 24 24\"\n                     fill=\"".concat(opts.fill, "\">\n                <path d=\"M18.92 6.01C18.72 5.42 18.16 5 17.5 5h-11c-.66 0-1.21.42-1.42 1.01L3 12v8c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-1h12v1c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-8l-2.08-5.99zM6.5 16c-.83 0-1.5-.67-1.5-1.5S5.67 13 6.5 13s1.5.67 1.5 1.5S7.33 16 6.5 16zm11 0c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zM5 11l1.5-4.5h11L19 11H5z\"/><path d=\"M0 0h24v24H0z\" fill=\"none\"/></svg>");
  if (opts.svgIcon) icon = opts.svgIcon; // opts alt property on leaflet markers best way to
  // allow for css selection img[alt=...]

  if (!opts.alt) opts.alt = "";
  var eOpts = {};
  if ('pane' in opts) eOpts.pane = opts.pane; // add position marker to map

  var size = [35, 35];
  if (opts.size) size = opts.size;
  var PosIcon = L.icon({
    iconUrl: 'data:image/svg+xml;base64,' + btoa(icon),
    iconSize: size
  });
  var pos_marker = L.marker(new L.LatLng(0, 0), _objectSpread({
    icon: PosIcon,
    alt: opts.alt
  }, eOpts));
  var pop = null;

  if (popup) {
    pop = L.popup({
      closeButton: false,
      className: 'poslabel'
    });
    pos_marker.bindPopup(pop, {
      autoClose: false,
      closeOnClick: false,
      // autopan false necessary to get everythin centered on map
      // unintuitive feature...
      autoPan: false
    });
  }

  if (map) pos_marker.addTo(map).openPopup(); // update marker method...

  var setPositionMarker = function setPositionMarker(latlon) {
    var content = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
    var panTo = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var newLatLng = new L.LatLng(latlon[0], latlon[1]);
    pos_marker.setLatLng(newLatLng); // todo update content in popup of marker...
    // i.e. do something with pop var here....

    if (panTo) map.panTo(newLatLng, {
      'animate': true,
      'duration': 0.25
    });
  };

  return {
    marker: pos_marker,
    setPositionMarker: setPositionMarker
  };
}

var _default = positionMarker;
exports["default"] = _default;