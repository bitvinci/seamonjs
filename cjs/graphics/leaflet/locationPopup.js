"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _leaflet = _interopRequireDefault(require("leaflet"));

var _dateformat = _interopRequireDefault(require("dateformat"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/* displays FC data in a popup in leaflet
   should be used with location forecasts */
function locationPopup(data, map) {
  var step = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  var time = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

  // check...
  if (!('forecasts' in data)) {
    console.error('locationPopup: no forecast data to show');
    return null;
  } // find first forecast with any data


  var firstFC = null;

  for (var fc in data.forecasts) {
    if (data.forecasts[fc].times.length > 0) {
      firstFC = fc;
      break;
    }
  }

  if (firstFC == null) {
    console.error("locationPopup: no data found");
    return null;
  } // get location


  var lat = data.forecasts[firstFC].lat;
  var lon = data.forecasts[firstFC].lon;
  var l = {
    lat: lat,
    lng: lon
  }; // get time,

  time = data.forecasts[firstFC].times[step];
  var jsTime = new Date(1000.0 * time); // loop through forecast data,
  // creating html summary for popup

  var c = ''; // add timestamp,

  c += '<span style="font-weight:bold;">' + (0, _dateformat["default"])(jsTime, "dd mmm HH:MM") + "</span>";
  c += '<hr style="margin:0;padding:0;"/>';

  for (var _fc in data.forecasts) {
    var fcData = data.forecasts[_fc];
    var units = fcData.units;
    c += "<span style='font-weight:bold;'>".concat(_fc.toUpperCase(), "</span><br/>");

    for (var par in fcData.members[_fc]) {
      var val = fcData.members[_fc][par][step];
      var unit = "";
      if (par in units) unit = units[par];
      c += "- <span style='font-weight:bold;'>".concat(par, ":</span> ").concat(val).concat(unit, "<br/>");
    }
    /* check if warning in data */


    if (fcData.warnings.length) console.warn('locationPopup:', _fc, fcData.warnings);
    /* if no data in this fc */

    if (fcData.times.length === 0) {
      c += "no data<br/>";
    }
  }

  var popup = _leaflet["default"].popup().setLatLng(l).setContent(c).openOn(map);
}

var _default = locationPopup;
exports["default"] = _default;