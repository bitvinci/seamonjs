"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _leaflet = _interopRequireDefault(require("leaflet"));

require("leaflet-arc");

var _styles = require("../../styles/styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// plots path based on the forecast lat lon values.
// this path only extends as far as the forecast reaches,
// but is one2one with the forecast data
function path(data, model, map) {
  var opts = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  // collect forecast points
  var lats = data.forecasts[model].lats;
  var lons = data.forecasts[model].lons;
  if (!('pane' in opts)) opts.pane = null; // inject critical values and style coloring
  // for lines if not provided with opts...

  if (!('critical' in opts)) {
    var _generateCritical = (0, _styles.generateCritical)(data),
        critical = _generateCritical.critical,
        encs = _generateCritical.encs;

    opts['critical'] = critical;
    opts['criticalStyle'] = _styles.stylesState.criticalStyle;
  } // line between points drawn here


  var fg = __line_feature(lats, lons, opts); // add to map if not null


  if (map) fg.addTo(map);
  return fg;
} // Great circle arc midpoint calculation


function __midpoint(p1, p2) {
  var deg2rad = Math.PI / 180.0;
  var rad2deg = 1.0 / deg2rad;
  var lon1 = p1.lng * deg2rad;
  var lat1 = p1.lat * deg2rad;
  var lon2 = p2.lng * deg2rad;
  var lat2 = p2.lat * deg2rad;
  var dlon = lon2 - lon1;
  var Bx = Math.cos(lat2) * Math.cos(dlon);
  var By = Math.cos(lat2) * Math.sin(dlon);
  var lat = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By));
  var lon = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);
  return new _leaflet["default"].LatLng(lat * rad2deg, lon * rad2deg);
}

function __line_feature(lats, lons, opts) {
  var fg = _leaflet["default"].featureGroup();

  var bgLines = [];
  var fgLines = [];

  for (var i = 1; i < lats.length; i++) {
    var p0 = new _leaflet["default"].LatLng(lats[i - 1], lons[i - 1]);
    var p1 = new _leaflet["default"].LatLng(lats[i], lons[i]); // default style

    var s0 = {
      color: 'black',
      weight: 3
    };
    var s1 = {
      color: 'black',
      weight: 3
    }; // critical array override

    if ('critical' in opts) {
      var c0 = opts.critical[i - 1];
      var c1 = opts.critical[i];
      if (c0 in opts.criticalStyle) s0 = Object.assign(s0, opts.criticalStyle[c0]);
      if (c1 in opts.criticalStyle) s1 = Object.assign(s1, opts.criticalStyle[c1]);
    } // has bgLine in opts,


    var hasBgLine = false;

    if ('bgLine' in opts) {
      hasBgLine = true;
      opts.bgLine.className = 'seamon-path-element-backdrop';
    } // additional line opts,


    var eOpts = {};
    if (opts.pane) eOpts.pane = opts.pane; // only add points that are different

    if (p0.lat !== p1.lat || p0.lng !== p1.lng) {
      // background line
      if (hasBgLine) {
        bgLines.push(_leaflet["default"].Polyline.Arc(p0, p1, _objectSpread(_objectSpread({}, opts.bgLine), eOpts)));
      } // foreground line
      //// split into two segments to interpolate between colors,
      //// only if color code is different between the points...


      if (s0.color !== s1.color) {
        var pMid = __midpoint(p0, p1); // first half


        var line1 = _leaflet["default"].Polyline.Arc(p0, pMid, _objectSpread({
          color: s0.color,
          weight: s0.weight,
          opacity: 1,
          className: 'seamon-path-element',
          dataIndex: i - 1
        }, eOpts)).on('mouseover', function (e) {
          if ('hover' in opts) {
            var di = e.target.options.dataIndex;
            opts.hover(di);
          }
        });

        fgLines.push(line1); // second half

        var line2 = _leaflet["default"].Polyline.Arc(pMid, p1, _objectSpread({
          color: s1.color,
          weight: s1.weight,
          opacity: 1,
          className: 'seamon-path-element',
          dataIndex: i
        }, eOpts)).on('mouseover', function (e) {
          if ('hover' in opts) {
            var di = e.target.options.dataIndex;
            opts.hover(di);
          }
        });

        fgLines.push(line2);
      } else {
        var line = _leaflet["default"].Polyline.Arc(p0, p1, _objectSpread({
          color: s0.color,
          weight: s0.weight,
          opacity: 1,
          className: 'seamon-path-element',
          dataIndex: i - 1
        }, eOpts)).on('mouseover', function (e) {
          if ('hover' in opts) {
            var di = e.target.options.dataIndex;
            opts.hover(di);
          }
        });

        fgLines.push(line);
      }
    }
  } // insert segments in correct draw order


  bgLines.forEach(function (x) {
    x.addTo(fg);
  });
  fgLines.forEach(function (x) {
    x.addTo(fg);
  });
  return fg;
}

var _default = path;
exports["default"] = _default;