"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("./route"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/* get forecast with drive route method */
function gca(waypts, params) {
  var opts = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  return (0, _route["default"])(waypts, params, "gca", opts);
}

var _default = gca;
exports["default"] = _default;