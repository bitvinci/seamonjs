"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _errors = _interopRequireDefault(require("../errors"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/* basic forecast query */
function route(waypts, params, solver) {
  var opts = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};
  // force default opts
  if (!opts.server) opts.server = 'https://api.seamon.io';
  if (!opts.token) opts.token = '';
  if (!opts.save) opts.save = false;
  if (!opts.allowMissingPar) opts.allowMissingPar = false; // compose URL query - -

  var url = opts.server + "/route?solver=".concat(solver, "&waypts=");
  waypts.forEach(function (x) {
    url += "(".concat(x[0], ";").concat(x[1], "),");
  });
  url = url.slice(0, -1); // add forecast parameter selection

  url += "&params=";
  params.forEach(function (x) {
    url += "".concat(x, ",");
  }); // add token

  url += "&token=" + opts.token; // add missing pars allowed, if so,

  if (opts.allowMissingPar) url += "&allow_missing_par";
  console.log("query: " + url); // query seamon

  return fetch(url)["catch"](function (e) {
    console.error("fetch had error in route.".concat(solver), e);
    return Promise.reject(new Error("failed to reach seamon-api service - check internet connection"));
  }).then(function (resp) {
    // check here for unexpected HTTP errors . e.g. 502
    if ([502, 504].includes(resp.status)) {
      return Promise.reject(new Error("".concat(resp.status, " gateway error - service may be down - admin will be notified")));
    } // all ok so pass on...


    return resp;
  }).then(function (resp) {
    return resp.json();
  }) // remember... .then() is sugar for await...
  .then(function (json) {
    if ('error' in json) {
      // so this is an API error, so
      // pass rejected promise with API error
      return Promise.reject((0, _errors["default"])(json));
    }

    return json;
  });
}

var _default = route;
exports["default"] = _default;