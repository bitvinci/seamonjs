"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

// define this api error type
function api_error(json) {
  var err = new Error(json.error.message);
  err.code = json.error.code;
  err.apiError = true;
  return err;
}

var _default = api_error;
exports["default"] = _default;