import SeaMon from '../esm/main.js';
import L from 'leaflet';
import dateFormat from 'dateformat';
/* regenerator runtime is a tool with parceljs that adds support 
   for async function seemingly required for main app code...
   Probably not required in other production environments */
import 'regenerator-runtime/runtime';

let show = document.getElementById('showData');
let timeDisplay = document.getElementById('showTime');

let mymap = L.map('mapid').setView([51.505, -0.09], 13);
let tileURL = "";
//let tileURL = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
//tileURL = "https://tiles.windy.com/tiles/v9.0/grayland/{z}/{x}/{y}.png";
L.tileLayer(tileURL, {
    attribution: '',
    maxZoom: 8,
    tileSize: 512,
    zoomOffset: -1,
    zIndex: 1000,
    opacity: 1.0
}).addTo(mymap);

// create pane for fc maps...
// this allows controlling z-level wrt tileLayers...
mymap.createPane('level1Pane');
mymap.getPane('level1Pane').style.zIndex = 50;
mymap.createPane('level2Pane');
mymap.getPane('level2Pane').style.zIndex = 100;
mymap.createPane('level3Pane');
mymap.getPane('level3Pane').style.zIndex = 150;
mymap.createPane('level4Pane');
mymap.getPane('level4Pane').style.zIndex = 200;

// demo drive route
//getDriveDemo();
getSailingDemo();
getPathDemo();

// webgl demo,
let webglMap = SeaMon.graphics.webgl.map();
webglMap.coastlines();

//add vector coastlines
SeaMon.graphics.leaflet.coastlines(mymap, 
    [-60,45,20,75], 
    {
        style: {
            'color':'black',
            'weight': 2.0,
            'opacity': 0.7
        },
        pane: 'level1Pane'});

function showTime(t) {
    timeDisplay.innerHTML = dateFormat(t, "dd/mm HH:MM");
}

// add click show location FC listener...
clickListener();
function clickListener() {
    mymap.on('click', function(e) {        
        var loc = e.latlng;
        // get data
        getLocation(loc);
    });
}

function getLocation(l) {
    let params = ['igb|wind', 'gust', 'temp', 'copwav|wave', 'mgwave|wave','copmyo|sst'];
    SeaMon.location(
        [l.lat,l.lng],
        'now', null, params,
        {token:'A5C2BX73',
         limit: 1})
    .then(data => {
        console.log('got data', data);
        SeaMon.graphics.leaflet.locationPopup(data, mymap);

    })
    .catch(err => {
        console.error('caught exception:', err);
    });
}

function showData(i) {
    let icons = document.getElementById('tooltip');
    // reset fc data in fc icons data tooltip
    icons.innerHTML = "";

    // grab handles on data
    let units = window.data.forecasts.igb.units;
    let igb = window.data.forecasts.igb.members.igb;
    let lats = window.data.forecasts.igb.lats;
    let lons = window.data.forecasts.igb.lons;
    let times = window.data.forecasts.igb.times;

    // loop through fc data and add icons/values to tooltip
    for(let w in igb) {
        let value = igb[w][i];
        let style = SeaMon.styles.generateStyle(value, w);
        let icon = SeaMon.styles.getWeatherIcon(w,
            {fill:style.color,
            stroke:style.color});
        let fValue = SeaMon.styles.formatValue(value, w);
        let unit = "";
        if(w in units) unit = units[w];
        let parHtml = `<div class="tooltip-parameter">${icon}${fValue}${unit}</div>`;
        icons.innerHTML = icons.innerHTML + parHtml;
    }
    // set marker,
    window.setPositionMarker([lats[i],lons[i]]);

    // lookup nearest map time stamp for this ship/car position,
    // if fc map data available...
    if(window.fcImagePlayer) {
        let t = times[i];
        let idx = window.fcImagePlayer.lookup(t);
        // request to flip map image to this index...
        window.setFC(idx);
    }
    // show time stamp...
    showTime(new Date(times[i]*1000.0));
}

function getPathDemo() {
    SeaMon.path(
        ['Reykjavik','Selfoss'],
        "initial_time",
        "end_time",
        ['igb|wind'],
        'gca',
        {token:'A5C2BX73'}
    )
    .then(data=>{
        console.log("path demo result", data);
    })
}

function getSailingDemo() {
    let params = [
        'igb|wind', 'gust', 'temp', 
        'rainr', 'snowr', 'vis'
    ];

    SeaMon.route.gca([['Reykjavik','initial_time'],
        ['64.28;-23.46','32kt'],
        ['64.67;-24.50','32kt'],
        ['65.8;-10.0','32kt']],
        params, {token:'A5C2BX73'})
    .then(data => {
        console.log("got data", data);

        window.crit = SeaMon.styles.generateCritical(data);
        window.data = data;

        //let featureGroup1 = SeaMon.graphics.leaflet.waypoints(data, mymap);

        //mymap.fitBounds(featureGroup1.getBounds());

        let path = SeaMon.graphics.leaflet.forecastPath(data, 'igb', null,
                                {
                                    pane: 'level3Pane',
                                    bgLine:{color: 'black',
                                            weight: 8,
                                            opacity: 1 },
                                    hover: showData}).addTo(mymap);

        // plot warning markers to map
        let wmarkers = SeaMon.graphics.leaflet.warningMarkers(data, 'igb', null, {pane: 'level4Pane'})
                              .addTo(mymap);
        
        // position marker
        // get a ship marker to use...
        let shipSVG = SeaMon.styles.getIcon("ship", {fill:"blue",stroke:"white"});
        let marker = SeaMon.graphics.leaflet.positionMarker(null,{size:[25,25], svgIcon: shipSVG, pane: 'level4Pane'});
        marker.marker.addTo(mymap).openPopup();
        window.setPositionMarker = marker.setPositionMarker;
    })
    .catch(err => {
        console.error('caught exception:', err);
    });
}

function getDriveDemo() {
    let params = [
        'igb|wind', 'gust', 'temp', 
        'rainr', 'snowr', 'road', 'cloud', 'vis'
    ];
    SeaMon.route.road([['Reykjavik','initial_time'], ['Akureyri','80kph']],
    params, {token:'A5C2BX73'})
    .then(data => {
        console.log("got data", data);

        window.crit = SeaMon.styles.generateCritical(data);
        window.data = data;

        let featureGroup1 = SeaMon.graphics.leaflet.waypoints(data, mymap);

        mymap.fitBounds(featureGroup1.getBounds());

        let featureGroup2 = SeaMon.graphics.leaflet.forecastPath(data, 'igb', mymap,
                                {
                                    bgLine:{ color: 'black',
                                            weight: 8,
                                            opacity: 1 },
                                    hover: showData});
        // plot warning markers to map
        let wmarkers = SeaMon.graphics.leaflet.warningMarkers(data, 'igb', mymap);
        // position marker
        let marker = SeaMon.graphics.leaflet.positionMarker(mymap,{size:[20,20]});
        window.setPositionMarker = marker.setPositionMarker;
    })
    .catch(err => {
        console.error('caught exception:', err);
    });
}

function clearVectors() {
    // remove any previous layers,
    if(window.fcVectors) {
        console.log("removing previous windbarbs");
        window.fcVectors.forEach(x=>{
            mymap.removeLayer(x);
        });
        window.fcVectors = null;
    }
}
function clearImagePlayer() {
    // remove previous image overlay...
    if(window.fcImagePlayer) {
        window.fcImagePlayer.remove(mymap);
        window.fcImagePlayer = null;
    }
}

// master get forecast function,
// preloads low res map, calls maps 
// and vector maps as appropriate...
function getFC(model, par, range) {
    // set grid for mgwave, igb, copmyo
    let gridll = [-50, 72, 20, 50, 25];
    if(model === "copwav") {
        gridll = [-50, 72, 20, 50, 9.0];
    }
    if(model === "igb") {
        gridll = [-36, 69, -10, 61, 2.5];
    }
    if(model === "copmyo") {
        gridll = [-36, 72, 0, 60, 9.0];
    }

    /****************************************/
    // first get quick low res image (just first time step)
    let lowGridll = [...gridll];
    lowGridll[4] = lowGridll[4]*5;
    let lowRange = "0h";
    getMapImages(model, par, lowRange, lowGridll);

    /****************************************/
    // get high res full images
    getMapImages(model, par, range, gridll);

    /****************************************/
    // get wind arrows if required...
    if(par === "wind") {
        let gridllVector = [...gridll];
        gridllVector[4] = gridllVector[4]*12; // thinner grid for vectors
        let opts = {pane: 'level1Pane',
                           opacity: 0.7,
                           weight: 2.0}
        getVectorData(model, null, 'wdir', range, gridllVector, opts);
    }
    // call any associated vector/arrow data
    else if(par === "sst") {
        let gridllVector = [...gridll];
        gridllVector[4] = gridllVector[4]*3; // thinner grid for vectors
        let featherOpacity = 0.15; // feather opacity up to this amp...
        let length = 15; // adjust length by 5x
        let opts = {pane: 'level1Pane',
                           opacity: 0.7,
                           weight: 2.0,
                           length: length,
                           featherOpacity: featherOpacity,
                           maxAmp: 2.0};

        getVectorData(model, 'meancurr', 'meancurrdir', range, gridllVector, opts);
    }
    // else clear any vector layer...
    else {
        // remove any previous vector layers,
        clearVectors();
    }
}

async function getVectorData(model, amp, dir, range, gridll, opts={}) {
    let server = 'https://api.seamon.io';
    //server = 'http://0.0.0.0:1706';

    let fetchPars = [model + '|' + dir];
    if(amp) fetchPars.push(amp);

    let stop = "prev_plus_" + range;
    let avLat = (gridll[1]+gridll[3])/2.0
    SeaMon.map(
        gridll,
        "initial_time",
        stop,
        fetchPars,
        {
            token:'A5C2BX73',
            server: server,
            proj4: `+proj=merc +lat_ts=${avLat}`,
            view: 'json', // raw data in json
            latlons: true // required for leaflet vector helper...
        })
    .then(data => {
        console.log('got data', data);

        // remove any previous vector layers,
        clearVectors();

        let vectorPlayer = SeaMon.graphics.leaflet.vectorPlayer(data, model, amp, dir, mymap, opts);
        window.vectorsIdx = 0;
        // store in window object,
        window.fcVectorPlayer = vectorPlayer;
    })
    .catch(err => {
            console.error('caught exception:', err);
    });
}

async function getMapImages(model, par, range, gridll) {

    // seamon server target...
    let server = 'https://api.seamon.io';

    // some color scales...
    let scales = "seaicef:(0:black,100:white),cloud:(0:black,100:white),chl:(0:blue,0.3:green,0.6:yellow,1.2:orange,3.5:red),phyc:(0:blue,1:green,2:yellow,4:orange,10:red)";
    // set scale interpolation option...
    let scaleInterpolation = 'linear';
    if (par === 'wind' ||
        par === 'temp' ) {
        scaleInterpolation = 'step';
    }

    let stop = "prev_plus_" + range;
    let avLat = (gridll[1]+gridll[3])/2.0
    SeaMon.map(
            gridll,
            "initial_time",
            stop,
            [model+"|"+par],
            {
                token:'A5C2BX73',
                server: server,
                proj4: `+proj=merc +lat_ts=${avLat}`,
                view: 'jsonimage', // json-wrapped base64 images
                scales: scales,
                scaleInterpolation: scaleInterpolation,
                antialias: 2
            })
    .then(data => {
            console.log('got data', data);

            // clear any previous image player from map...
            clearImagePlayer();

            // create new image player
            let player = SeaMon.graphics.leaflet.imagePlayer(data, model, par, mymap, 
                {pane:'level1Pane'});
 
            // store player in window object
            window.fcImagePlayer = player;

            // zoom map to fit bounds...
            mymap.fitBounds(player.getBounds());
    })
    .catch(err => {
            console.error('caught exception:', err);
    });
}

window.getFCHandlerWebgl = (model, par, range) => {
    let gridll = [-45,70,-10,61, 2.5];
    if(par === "sst") {
        gridll = [-50,75,10,50,9.0];
    }
    getMapImagesWebgl(model, par, range, gridll);
};

async function getMapImagesWebgl(model, par, range, gridll) {

    // seamon server target...
    let server = 'https://api.seamon.io';

    // some color scales...
    let scales = "seaicef:(0:black,100:white),cloud:(0:black,100:white),chl:(0:blue,0.3:green,0.6:yellow,1.2:orange,3.5:red),phyc:(0:blue,1:green,2:yellow,4:orange,10:red)";
    // set scale interpolation option...
    let scaleInterpolation = 'linear';
    if (par === 'wind' ||
        par === 'temp' ) {
        scaleInterpolation = 'step';
    }

    let stop = "prev_plus_" + range;

    SeaMon.map(
        gridll,
        "initial_time",
        stop,
        [model+"|"+par],
        {
            token: 'A5C2BX73',
            server: server,
            proj4: "proj=longlat",
            view: 'jsonimage', // json-wrapped base64 images
            scales: scales,
            scaleInterpolation: scaleInterpolation,
            antialias: 2
    })
    .then(data => {
        console.log('got data', data);

        // test webglmap map tile...
        console.log("plotting webgl");
        let bbox = data.forecasts[model].bboxll;
        let img = 'data:image/png;base64,' + data.forecasts[model].members[model][par][0];
        webglMap.imageTile(img, bbox);
     })
    .catch(err => {
            console.error('caught exception:', err);
    });
}

window.getFCHandler = (model, par, range) => {
    window.stopFC();
    getFC(model, par, range);
};

// set fc map to a particular time index
window.setFC = (idx) => {
    window.stopFC();
    window.fcImagePlayer.setIndex(idx);
    window.fcVectorPlayer.setIndex(idx);
}

window.playFC = () => {
    let fcImagePlayer = window.fcImagePlayer;
    let fcVectorPlayer = window.fcVectorPlayer;
    let eventLoop = window.playEventLoop;
    if(fcImagePlayer && !eventLoop) {
        console.log("Playing FC");
        window.playEventLoop = setInterval(() => {
            fcImagePlayer.next();
            let idx = fcImagePlayer.idx;
            fcVectorPlayer.setIndex(idx);
            // set time stamp display on page...
            showTime(fcImagePlayer.timeJS());            
        }, 500);
    }
};

window.stopFC = () => {
    if(window.playEventLoop) {
        console.log("Stopping FC");
        clearInterval(window.playEventLoop);
        window.playEventLoop = null;
    }
}
